package esei.uvigo.es.lilla.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import esei.uvigo.es.lilla.GmailSend;
import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.R;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.TareaNotificada;

/**
 * Created by Oscar Campos del Río on 15/03/2017.
 */

/**
 * Receiver para reintento de envio de correo al cuidador principal.
 */
public class ReintentoEnvioCorreo extends WakefulBroadcastReceiver {
    private Context context;
    private TareaNotificada tareaNotificada;


    @Override
    public void onReceive(Context c, android.content.Intent i) {
        this.context=c;
        Bundle params = i.getExtras();
        tareaNotificada = ((Lilla) context.getApplicationContext()).tareasBD.getTareaNotificada(params.getInt("id"));
        Intent intentEmail = new Intent(context, GmailSend.class);
        intentEmail.putExtra("titulo", context.getResources().getString(R.string.titulo_aviso_email_cp) + " " + tareaNotificada.getTitulo());
        intentEmail.putExtra("cuerpo", componerCuerpoEmail());
        intentEmail.putExtra("correo_tarea", true);
        intentEmail.putExtra("id_tarea", tareaNotificada.getId_tarea_notificada());
        intentEmail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK + Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intentEmail);

    }

    /**
     * Composicion de  cuerpo de correo.
     * @return cadena de caracteres con cuerpo de correo.
     */
    private String componerCuerpoEmail(){
        String ret;

        Calendar fecha_hora =  Calendar.getInstance(TimeZone.getDefault());
        fecha_hora.setTimeInMillis(tareaNotificada.getFechaHoraTarea());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        ret=    context.getResources().getString(R.string.titulo_cuerpo_email_cp) + "\n" +
                tareaNotificada.getTitulo() + "\n" +
                tareaNotificada.getDescripcion()+ "\n" +
                (String.format("%02d", fecha_hora.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", fecha_hora.get(Calendar.MINUTE))) + " " +
                (dateFormatter.format(fecha_hora.getTime())) + " " +  TimeZone.getDefault().getDisplayName();

        return ret;
    }

}
