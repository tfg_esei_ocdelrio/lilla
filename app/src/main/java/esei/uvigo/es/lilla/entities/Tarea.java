package esei.uvigo.es.lilla.entities;

import android.os.Build;

import java.security.InvalidParameterException;

import static java.util.Objects.requireNonNull;

/**
 * Created by Oscar Campos del Rio on 25/01/2017.
 */

/**
 * Super clase  con datos de las tareas a notificar mediante alarna.
 */
public class Tarea {
    /**
     * Atributo identificador de la tarea (Si valor -1 se trata de una tarea nueva).
     */
    private int id_tarea;

    /**
     * Atributo cadena de caracteres con el titulo de la tarea.
     */
    private String titulo;

    /**
     * Atributo cadena de caracteres con la drescripción de la tarea..
     */
    private String descripcion;

    /**
     * Atributo identificador del nivel de importancia de la tarea.
     */
    private int nivel_importancia;

    public Tarea(int id_tarea, String titulo, String descripcion, int id_nivel ) {
        this.setId_tarea(id_tarea);
        this.setTitulo(titulo);
        this.setDescripcion(descripcion);
        this.setNivel_importancia(id_nivel);
    }

    public int getId_tarea() {
        return id_tarea;
    }

    public void setId_tarea(int id_tarea) throws InvalidParameterException {
        if(id_tarea>=-1)
            this.id_tarea = id_tarea;
        else
            throw new InvalidParameterException();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) throws NullPointerException {
        if(Build.VERSION.SDK_INT >= 19)
            this.titulo = requireNonNull(titulo, "titulo can not be null");
        else {
            this.titulo=titulo;
            if(this.titulo==null) throw new NullPointerException();
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        if(descripcion==null){
            this.descripcion="";
        } else
            this.descripcion=descripcion;
    }


    public int getNivel_importancia() {
        return nivel_importancia;
    }

    public void setNivel_importancia(int id_nivel) {
        if(id_nivel>=0)
            this.nivel_importancia = id_nivel;
        else
            throw new InvalidParameterException();
    }


    @Override
    public String toString() {
        return "Tarea{" +
                "id_tarea=" + id_tarea +
                ", titulo='" + titulo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", nivel_importancia=" + nivel_importancia +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tarea other = (Tarea) obj;
        if (id_tarea != other.id_tarea)
            return false;
        return true;
    }

}
