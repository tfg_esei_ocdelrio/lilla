package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 13/03/2017.
 */

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import java.text.SimpleDateFormat;

import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.clans.fab.FloatingActionButton;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.TareaPuntual;


/**
 * Activity parar crear o editar tarea puntual.
 */
public class EdicionTareaPuntual extends AppCompatActivity {

    private int id;
    private TareaPuntual tarea;
    private FloatingActionButton fActionButtonGuardar, fActionButtonCancelar;
    private SimpleCursorAdapter spinnerAdapterNivelImportanciaTareaPuntual;
    private EditText etTituloTareaPuntual, etDescripcionTareaPuntual;
    private Spinner spinnerNivelImportanciaTareaPuntual;
    private TextView tvHoraTareaPuntual, tvFechaTareaPuntual;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Calendar fechaHoraTv = Calendar.getInstance(TimeZone.getDefault());

    /* TimePickerDialog para hora de tarea. */
    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay,
                              int minute) {
            fechaHoraTv.set(Calendar.HOUR_OF_DAY, hourOfDay);
            fechaHoraTv.set(Calendar.MINUTE, minute);
            tvHoraTareaPuntual.setText(String.format("%02d", fechaHoraTv.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", fechaHoraTv.get(Calendar.MINUTE)));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edicion_tarea_puntual);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("_id",-1);


        /* DatePickerDialog para fecha de tarea. */
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Calendar newCalendar = Calendar.getInstance(TimeZone.getDefault());
        datePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                fechaHoraTv.set(year, monthOfYear, dayOfMonth);
                tvFechaTareaPuntual.setText(dateFormatter.format(fechaHoraTv.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        etTituloTareaPuntual =  (EditText) findViewById(R.id.et_titulo_tarea_puntual);
        etDescripcionTareaPuntual = (EditText) findViewById(R.id.et_descripcion_tarea_puntual);
        tvHoraTareaPuntual =  (TextView) findViewById(R.id.tv_hora_tarea_puntual);
        fActionButtonGuardar = (FloatingActionButton) findViewById(R.id.material_design_floating_action_guardar);
        fActionButtonCancelar = (FloatingActionButton) findViewById(R.id.material_design_floating_action_cancelar);
        spinnerNivelImportanciaTareaPuntual = (Spinner)findViewById(R.id.spinner_nivel_importancia_tarea_puntual);
        tvFechaTareaPuntual =  (TextView)findViewById(R.id.tv_fecha_tarea_puntual);

        /* Spinner cargado con niveles de importancia de tarea. */
        String[] queryCols=new String[]{"_id","NOMBRE"};
        String[] adapterCols=new String[]{"NOMBRE"};
        int[] adapterRowViews=new int[]{android.R.id.text1};
        spinnerAdapterNivelImportanciaTareaPuntual=new SimpleCursorAdapter(this,
                android.R.layout.simple_spinner_item,
                ((Lilla) getApplication()).tareasBD.getAllNiveles(),
                adapterCols,
                adapterRowViews,
                0);
        spinnerAdapterNivelImportanciaTareaPuntual.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNivelImportanciaTareaPuntual.setAdapter(spinnerAdapterNivelImportanciaTareaPuntual);
        spinnerNivelImportanciaTareaPuntual.setSelection(0);

        /*
        * Si obtenemos id=-1 de valores de intent pasado se trata de una nueva tarea, si obtenemos
        * un id>-1 se trata de la modificación de una tarea, se obtendran los datos para mostrarlos.
        */
        if(id != -1){
            tarea =  ((Lilla) getApplication()).tareasBD.getTareaPuntual(id);
            etTituloTareaPuntual.setText(tarea.getTitulo());
            etDescripcionTareaPuntual.setText(tarea.getDescripcion());
            spinnerNivelImportanciaTareaPuntual.setSelection(tarea.getNivel_importancia()-1);
            fechaHoraTv.setTimeInMillis(tarea.getFechaHora());
            tvFechaTareaPuntual.setText(dateFormatter.format(fechaHoraTv.getTime()));
            tvHoraTareaPuntual.setText(String.format("%02d", fechaHoraTv.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", fechaHoraTv.get(Calendar.MINUTE)));
        }else{
            fechaHoraTv.setTimeInMillis(System.currentTimeMillis());
            tvFechaTareaPuntual.setText(dateFormatter.format(fechaHoraTv.getTime()));
            tvHoraTareaPuntual.setText(String.format("%02d", fechaHoraTv.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", fechaHoraTv.get(Calendar.MINUTE)));
        }

        /*listener textView de hora para iniciar TimePickerDialog para hora de tarea. */
        tvHoraTareaPuntual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                new TimePickerDialog(EdicionTareaPuntual.this,
                        t,
                        fechaHoraTv.get(Calendar.HOUR_OF_DAY),
                        fechaHoraTv.get(Calendar.MINUTE),
                        true).show();
            }

        });


        /*listener textView de fecha para iniciar DatePickerDialog para fecha de tarea. */
        tvFechaTareaPuntual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                datePickerDialog.show();
            }

        });

        /*listener fabbutton para guardar datos de tarea */
        fActionButtonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                /* Se comprueba que el título no este vacío. */
                if(TextUtils.isEmpty(etTituloTareaPuntual.getText().toString().trim())) {
                    etTituloTareaPuntual.setError(getString(R.string.CampoTituloError));
                    return;
                }


                /* Se comprueba que la fecha y hora introducida en superior a la actual */
                fechaHoraTv.set(Calendar.SECOND,0);
                fechaHoraTv.set(Calendar.MILLISECOND,0);
                if(System.currentTimeMillis() >= (fechaHoraTv.getTimeInMillis())){
                    Snackbar.make(v, getString(R.string.FechaHoraErronea), Snackbar.LENGTH_SHORT)
                            .setDuration(4000)
                            .show();
                    return;
                }


                tarea = new TareaPuntual(
                        id,
                        etTituloTareaPuntual.getText().toString().trim(),
                        etDescripcionTareaPuntual.getText().toString().trim(),
                        fechaHoraTv.getTimeInMillis(),
                        (int) (spinnerNivelImportanciaTareaPuntual.getSelectedItemId())
                        );


                /*
                * Si obtenemos id=-1 se trata de una nueva tarea, si obtenemos un id>-1 se trata de
                * la modificación de una tarea. Registraremos los datos en la base de datos.
                */
                if(tarea.getId_tarea()==-1){
                    id=nuevaTarea();
                } else {
                    actualizarTarea();
                }


                /* Se establece alarma */
                FunctionsAlarmManager.establecerAlarmaInternaPuntual(tarea, getApplicationContext());

                 /* Actualizamos ReciclerViews de tareas programadas */
                if(ListaTareasProgramadas.tareasAdapter != null ) {
                    ListaTareasProgramadas.tareasAdapter.setList( ((Lilla) getApplication()).tareasBD.getAllTareas());
                    ListaTareasProgramadas.tareasAdapter.notifyDataSetChanged();
                }


                finish();
            }
        });

        fActionButtonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edicion_tarea, menu);
        MenuItem accionBorrar = menu.findItem(R.id.accion_borrar);

        /* Si id!=1 se muestra la opcion de borrar tarea */
        if(id!=-1)
            accionBorrar.setVisible(true);
        else
            accionBorrar.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_borrar:
                this.borrarTarea();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Borrado de tarea que se esta modificando.
     */
    private void borrarTarea(){
        if(id==-1) {
            Snackbar.make(getCurrentFocus(), getString(R.string.BorradoErroneo), Snackbar.LENGTH_SHORT)
                    .setDuration(4000)
                    .show();
            return;
        }else {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.titulo_borrar_tarea))
                    .setMessage(getResources().getString(R.string.mensaje_borrar_tarea))
                    .setPositiveButton(getResources().getString(R.string.confirmar_alert_dialog), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ((Lilla) getApplication()).tareasBD.borrarTarea(id);
                            id=-1;
                             /* Actualizamos ReciclerViews de tareas programadas */
                            if(ListaTareasProgramadas.tareasAdapter != null ) {
                                ListaTareasProgramadas.tareasAdapter.setList(((Lilla) getApplication()).tareasBD.getAllTareas());
                                ListaTareasProgramadas.tareasAdapter.notifyDataSetChanged();
                            }
                            finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancelar_alert_dialog), null)
                    .show();
              }
    }

    /**
     * Se registra en base de datos nueva tarea puntual introducida.
     * @return id identificador de nueva tarea.
     */
    private int nuevaTarea(){
        tarea.setId_tarea( ((Lilla) getApplication()).tareasBD.nuevaTareaPuntual(tarea));
        return tarea.getId_tarea();
    }

    /** Se actualiza en base de datos la tarea puntual modificada. **/
    private void actualizarTarea(){
        ((Lilla) getApplication()).tareasBD.actualizaTareaPuntual(tarea);
    }

    /**
     * Si se esta modificando una tarea, ante onPause se restablece la alarma con datos anteriores.
     */
    @Override
    public void onPause() {
        super.onPause();
        if(id != -1) {
            FunctionsAlarmManager.establecerAlarmaInternaPuntual(tarea, getApplicationContext());
        }
    }

    /**
     * Si se esta modificando una tarea, ante onResume se elimina la alarma para que no salte alarma
     * mientras se edita la tarea.
     */
    @Override
    public void onResume() {
        super.onResume();
        if(id != -1) {
            FunctionsAlarmManager.borrarAlarmaInternaPuntual(tarea,getApplicationContext());
        }
    }


}

