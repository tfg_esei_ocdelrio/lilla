package esei.uvigo.es.lilla;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListLabelsResponse;
import com.google.api.services.gmail.model.Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.abstract_classes.FunctionsMail;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Oscar Campos del Río on 12/03/2017.
 * Based in example of @see <a href="https://developers.google.com/gmail/api/quickstart/android?authuser=1">Android Quickstart</a>
 */

public class GmailSend extends Activity implements EasyPermissions.PermissionCallbacks  {
    private GoogleAccountCredential mCredential;
    private String titulo;
    private String cuerpo;
    private int idTarea;
    private boolean correo_tarea;
    private Context context;


    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    private static final String[] SCOPES = { GmailScopes.GMAIL_SEND };
    private static final String PREF_ACCOUNT_NAME = "accountName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        Intent intent = getIntent();
        Bundle params = intent.getExtras();

        titulo = params.getString("titulo");
        cuerpo  = params.getString("cuerpo");
        correo_tarea = params.getBoolean("correo_tarea",false);
        idTarea = params.getInt("id_tarea",-1);


        /* Llamada api GMAIL */
        mCredential = GoogleAccountCredential.usingOAuth2(
               getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        getResultsFromApi();



    }

    /**
     * Intenta llamar a la API, después de verificar que se cumplen todas las condiciones previas.
     * Las precondiciones son:
     * -Google Play Services instalado en el terminar.
     * -Una cuenta fue selectionada.
     * -Dispositivo actualmente con acceso en linea.
     * Si alguna de las precondiciones no son satisfechas, la app solicitará al usuario segun sea
     * apropiado..
     */
    public void getResultsFromApi() {
        if (! isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
           chooseAccount();
        } else if (! isDeviceOnline()) {
            Toast toast = Toast.makeText(this, getString(R.string.error_no_conexion), Toast.LENGTH_LONG);
            toast.show();
            if(correo_tarea)
                FunctionsAlarmManager.establecerAlarmaReintentoEnvioCorreo(idTarea,context);

            setResult(RESULT_CANCELED, null);
            finish();
        } else {
            new MakeRequestTask(mCredential).execute();
        }

    }


    /**
     * Intenta establecer la cuenta utilizada con las credenciales de la API. Si un nombre de cuenta
     * fuer guardado previamente, se usará esa; de lo contrario se mostrará al usuario un picker
     * dialog de cuentas. Tenga en cuenta que la configuración de la cuenta para utilizar las
     * credenciales del objeto quiere que la aplicación tenga el permiso GET_ACCOUNTS, que se
     * soliticará si aún no está presente. La anotacion AfterPErmissionGranted indica que esta
     * función se volverá a ejecutar automáticamente cada vez que se conceda el permiso
     * GET_ACCOUNTS.
     */
    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                /* Comentar dialog el usuario puede elegir la cuenta. */
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Solicita el permiso GET_ACCOUNTS a través de un user dialog. */
            if (!EasyPermissions.permissionPermanentlyDenied(this, Manifest.permission.GET_ACCOUNTS)){
                Toast toast = Toast.makeText(context, getString(R.string.permiso_denegado), Toast.LENGTH_LONG);
                toast.show();
                finish();

            } else {
                EasyPermissions.requestPermissions(
                        this,
                        getString(R.string.dialog_get_accounts),
                        REQUEST_PERMISSION_GET_ACCOUNTS,
                        Manifest.permission.GET_ACCOUNTS);
            }
        }
    }

    /**
     * Se llamada cuando una actividad lanzada aquí ( específicamente AccountPicker y Authorization)
     * termina su ejecución, dando el requestCode con el que se inicio, el resultCode retornado y
     * cualquier información adicionál de el en data.
     * @param requestCode codigo que indica que resultado de activity es entrante.
     * @param resultCode codigo que indica el resultado de la activity.
     * @param data Intent (contiene información del resultado) devuelto por el resultado del a
     *             activity.
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast toast = Toast.makeText(this,getString(R.string.error_no_google_play), Toast.LENGTH_LONG);
                    toast.show();
                    setResult(RESULT_CANCELED, null);
                    finish();
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;
        }
    }

    /**
     * Respuesta a solicitudes de permisos en tiempo de ejecucoón para API 23 o superior
     * @param requestCode El código de solicitud pasado en
     *     requestPermissions(android.app.Activity, String, int, String[])
     * @param permissions Los permisos solicitados. Nunca null.
     * @param grantResults Los resultados de la concesión para los permisos correspondientes que son
     *  PERMISSION_GRANTED o PERMISSION_DENIED. Nunca null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        String permission = permissions[0];
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Toast toast = Toast.makeText(context, getString(R.string.permiso_denegado), Toast.LENGTH_LONG);
            toast.show();
            finish();
        }
        else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            EasyPermissions.onRequestPermissionsResult(
                    requestCode, permissions, grantResults, this);
        }

    }

    /**
     * Rellamada para cuando un permiso es concedido usando la libreria EasyPermissions.
     * @param requestCode El codigo de solicitud asociado con el permiso solicitado.
     * @param list La lista de permisos solicitados. Nunca null.
     */
    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // No hace nada.
    }

    /**
     * Rellamada para cuando un permiso es denegado usando la libreria EasyPermissions.
     * @param requestCode El codigo de solicitud asociado con el permiso solicitado
     * @param list La lista de permisos solicitados. Nunca null.
     */
    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        finish();
    }

    /**
     * Comprueba si el dispositivo tiene actualmente una conexión de red.
     * @return true si el dispositivo tiene conexión, por lo contrario devolvera false.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Comprueba que Google Play services APK esta instalado y actualizado
     * @return true si Google Play Services esta disponible y actualizado
     *     en el dispositivo, por lo contrario devolvera false.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(context);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Intento de resolver una instalación de los servicios de Google Play ausentes,
     * desactualizados, no válidos o deshabilitados mediante un user dialog, si es posible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(context);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            finish();
        }
    }


    /**
     * Muestra un error dialog mostrando que Google Play Services no se encuentran o están
     * desactualizados .showGooglePlayServicesAvailabilityErrorDialog
     * @param connectionStatusCode Código que describe la presencia (o la falta de)
     * Google Play Services en este dispositivo.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();

        Toast toast = Toast.makeText(context, getString(R.string.error_no_google_play), Toast.LENGTH_LONG);
        toast.show();
        setResult(RESULT_CANCELED, null);

    }

    /**
     * Una tarea asincrona que maneja la llamada a Gmail API.
     * Colocando las llamadas de la API  en su propia tarea garantiza que la interfaz de usuario
     * permanezca receptiva.
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        private com.google.api.services.gmail.Gmail mService = null;
        private Exception mLastError = null;

        MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.gmail.Gmail.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Lilla")
                    .build();
        }

        /**
         * Tarea segundo plano para llamar a Gmail API.
         * @param params no son necesarios parametros para esta tarea.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                sendEmail();
                return null;
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Obtener una lista de labels de Gmail vinculadas a la cuenta especificada.
         * @return Lista  de Strings con labels.
         * @throws IOException
         */
        private List<String> getDataFromApi() throws IOException {
            // Get the labels in the user's account.
            String user = "me";
            List<String> labels = new ArrayList<String>();
            ListLabelsResponse listResponse =
                    mService.users().labels().list(user).execute();
            for (Label label : listResponse.getLabels()) {
                labels.add(label.getName());
            }
            return labels;
        }

        /**
         * Envio de correo obteniendo direccion de envio de SharedPreferences, el titulo y cuerpo
         * de los paramestros pasados por extra en  el intent a la activity.
         * @throws IOException
         */
        private void sendEmail() throws IOException {
            // Get the labels in the user's account.
            MimeMessage mimeMessage = null;
            Message message = null;
            String userFrom = mCredential.getSelectedAccountName();
            try {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                String userTo =prefs.getString("correo_notificacion_cp", "");
                if(!userTo.isEmpty())
                    mimeMessage = FunctionsMail.createEmail(userTo,userFrom,
                            titulo,
                            cuerpo);
                else {
                    Toast toast = Toast.makeText(context, getString(R.string.error_no_email_cp), Toast.LENGTH_LONG);
                    toast.show();
                }
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            try {
                message = FunctionsMail.createMessageWithEmail(mimeMessage);
            }
            catch (MessagingException e) {
                e.printStackTrace();
            }
            try {
                FunctionsMail.sendMessage(mService, userFrom, mimeMessage);
                setResult(RESULT_OK, null);
                finish();
            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }


        /**
         * En pre ejecución del envio de correo se muestra toast indicando que se esta enviando
         * el correo.
         */
        @Override
        protected void onPreExecute() {
            Toast toast = Toast.makeText(context, getString(R.string.enviando_correo) , Toast.LENGTH_LONG);
            toast.show();

        }

        @Override
        protected void onPostExecute(List<String> output) {
            if (output == null || output.size() == 0) {
            } else {
                output.add(0, getString(R.string.datos_recuperados_api));
                Toast toast = Toast.makeText(context, TextUtils.join("\n", output), Toast.LENGTH_LONG);
                toast.show();
            }
        }

        /**
         * En caso de error se mostrará toast con error y se finalizará la actividad.
         */
        @Override
        protected void onCancelled() {

            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            REQUEST_AUTHORIZATION);
                } else {
                    Toast toast = Toast.makeText(context, getString(R.string.error_api)  + mLastError.getMessage(), Toast.LENGTH_LONG);
                    toast.show();
                    setResult(RESULT_CANCELED, null);
                    finish();
                }
            } else {
                Toast toast = Toast.makeText(context, getString(R.string.peticion_cancelada), Toast.LENGTH_LONG);
                toast.show();
                setResult(RESULT_CANCELED, null);
                finish();
            }
        }
    }
}
