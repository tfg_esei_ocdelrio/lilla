package esei.uvigo.es.lilla.entities;

import android.os.Build;

import java.security.InvalidParameterException;

import static java.util.Objects.requireNonNull;

/**
 * Created by Oscar Campos del Río on 25/01/2017.
 */

/**
 * Clase con nivel de importancia que pueden tener las tareas, la cual recogera caracteristicas de
 * notificaciones y apariencia.
 */
public class NivelImportanciaTarea {
    /**
     * Atributo identificador del nivel de importancia.
     */
    private int id_nivel;

    /**
     * Atributo cadena de caracteres con nombre del nivel de importancia.
     */
    private String nombre;

    /**
     * Atributo cantidad máxima de notificaciones de una TareaNotificada.
     */
    private int limite_notificaciones;

    /**
     * Atributo boleano que indica si se notificará al cuidador principal la tarea que no se
     * confirme si llega al limite de notificaciones (true=si, false=no).
     */
    private boolean aviso_cp;

    /**
     * Atributo cantidad de minutos a pasar entre notificación y notificación.
     */
    private int minutos;

    /**
     * Atributo cadena caracteres con valor alfanumérico con color representativo del nivel de
     * importancia,
     */
    private String color;

    public int getId_nivel() {
        return id_nivel;
    }

    public void setId_nivel(int id_nivel) throws InvalidParameterException {
        if(id_nivel>=0)
            this.id_nivel = id_nivel;
        else
            throw new InvalidParameterException();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) throws NullPointerException {
        if(Build.VERSION.SDK_INT >= 19)
            this.nombre = requireNonNull(nombre, "nombre can not be null");
        else {
            this.nombre=nombre;
            if(this.nombre==null) throw new NullPointerException();
        }
    }

    public int getLimite_notificaciones() {
        return limite_notificaciones;
    }

    public void setLimite_notificaciones(int limite_notificaciones) throws InvalidParameterException  {
        if(limite_notificaciones>=0)
            this.limite_notificaciones = limite_notificaciones;
        else
            throw new InvalidParameterException();
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) throws InvalidParameterException {
        if(minutos>=0)
            this.minutos = minutos;
        else
            throw new InvalidParameterException();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) throws NullPointerException {
        if(Build.VERSION.SDK_INT >= 19)
            this.color = requireNonNull(color, "color can not be null");
        else {
            this.color=color;
            if(this.color==null) throw new NullPointerException();
        }
    }

    public boolean isAviso_cp() {
        return aviso_cp;
    }

    public void setAviso_cp(boolean aviso_cp) {
        this.aviso_cp = aviso_cp;
    }

    public NivelImportanciaTarea(int id_nivel, String nombre, int limite_notificaciones, boolean aviso_cp, int minutos, String color) {
        this.setId_nivel(id_nivel);
        this.setNombre(nombre);
        this.setLimite_notificaciones(limite_notificaciones);
        this.setAviso_cp(aviso_cp);
        this.setMinutos(minutos);
        this.setColor(color);
    }

    @Override
    public String toString() {
        return "NivelImportanciaTarea{" +
                "id_nivel=" + id_nivel +
                ", nombre='" + nombre + '\'' +
                ", limite_notificaciones=" + limite_notificaciones +
                ", aviso_cp=" + aviso_cp +
                ", minutos=" + minutos +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NivelImportanciaTarea other = (NivelImportanciaTarea) obj;
        if (id_nivel != other.id_nivel)
            return false;
        return true;
    }
}
