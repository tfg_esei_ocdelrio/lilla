package esei.uvigo.es.lilla.entities;

/**
 * Created by Oscar Campos del Río on 25/01/2017.
 */

/**
 * Clase extendida de tarea que representa una tarea con un instante unico de realización.
 */
public class TareaPuntual extends Tarea {
    /**
     * Atributo representa hora y fecha en que debe ser realizada la tarea. Usado para
     * ejecución dela alarma (valor en milisegundos desde 01/01/1970).
     */
    private long fechaHora;

    public TareaPuntual(int id_tarea, String titulo, String descripcion, long fechaHora,
                        int id_nivel) {
        super(id_tarea, titulo, descripcion, id_nivel);
        this.setFechaHora(fechaHora);
    }

    public long getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(long fechaHora) {
            this.fechaHora = fechaHora;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "TareaPuntual{" +
                "fechaHora=" + fechaHora +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TareaPuntual other = (TareaPuntual) obj;
        if (this.getId_tarea() != other.getId_tarea())
            return false;
        return true;
    }


}
