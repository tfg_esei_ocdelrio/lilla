package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 13/03/2017.
 */

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.github.clans.fab.FloatingActionButton;

import java.util.Calendar;
import java.util.TimeZone;

import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.abstract_classes.FunctionsCalendar;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.TareaSemanal;

/**
 * Activity parar crear o editar tarea semanal.
 */
public class EdicionTareaSemanal extends AppCompatActivity  {


    private int id;
    private SimpleCursorAdapter spinnerAdapterNivelImportanciaTareaSemanal;
    private FloatingActionButton fActionButtonGuardar, fActionButtonCancelar;
    private TareaSemanal tarea;
    private EditText etTituloTareaSemanal, etDescripcionTareaSemanal;
    private Spinner spinnerNivelImportanciaTareaSemanal;
    private ToggleButton toggleButtonLunes, toggleButtonMartes, toggleButtonMiercoles, toggleButtonJueves,
            toggleButtonViernes, toggleButtonSabado, toggleButtonDomingo;
    private TextView twvHoraTareaSemanal;

    /* TimePickerDialog para hora de tarea. */
    Calendar horaTv =Calendar.getInstance(TimeZone.getDefault());
    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay,
                              int minute) {
            horaTv.set(Calendar.HOUR_OF_DAY, hourOfDay);
            horaTv.set(Calendar.MINUTE, minute);
            twvHoraTareaSemanal.setText(String.format("%02d", horaTv.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", horaTv.get(Calendar.MINUTE)));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edicion_tarea_semanal);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("_id",-1);


        etTituloTareaSemanal = (EditText) findViewById(R.id.et_titulo_tarea_semanal);
        etDescripcionTareaSemanal = (EditText) findViewById(R.id.et_descripcion_tarea_semanal);
        spinnerNivelImportanciaTareaSemanal = (Spinner)findViewById(R.id.spinner_nivel_importancia_tarea_semanal);
        toggleButtonLunes = (ToggleButton)findViewById(R.id.toggleButton_lunes);
        toggleButtonMartes = (ToggleButton)findViewById(R.id.toggleButton_martes);
        toggleButtonMiercoles = (ToggleButton)findViewById(R.id.toggleButton_miercoles);
        toggleButtonJueves = (ToggleButton)findViewById(R.id.toggleButton_jueves);
        toggleButtonViernes = (ToggleButton)findViewById(R.id.toggleButton_viernes);
        toggleButtonSabado = (ToggleButton)findViewById(R.id.toggleButton_sabado);
        toggleButtonDomingo = (ToggleButton)findViewById(R.id.toggleButton_domingo);
        twvHoraTareaSemanal = (TextView)findViewById(R.id.tv_hora_tarea_semanal);
        fActionButtonGuardar = (FloatingActionButton) findViewById(R.id.material_design_floating_action_guardar);
        fActionButtonCancelar = (FloatingActionButton) findViewById(R.id.material_design_floating_action_cancelar);

        /* Spinner cargado con niveles de importancia de tarea. */
        String[] queryCols=new String[]{"_id","NOMBRE"};
        String[] adapterCols=new String[]{"NOMBRE"};
        int[] adapterRowViews=new int[]{android.R.id.text1};
        spinnerAdapterNivelImportanciaTareaSemanal =new SimpleCursorAdapter(this,
                android.R.layout.simple_spinner_item,
                ((Lilla) getApplication()).tareasBD.getAllNiveles(),
                adapterCols,
                adapterRowViews,
                0);
        spinnerAdapterNivelImportanciaTareaSemanal.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNivelImportanciaTareaSemanal.setAdapter(spinnerAdapterNivelImportanciaTareaSemanal);
        spinnerNivelImportanciaTareaSemanal.setSelection(0);

        /*
        * Si obtenemos id=-1 de valores de intent pasado se trata de una nueva tarea, si obtenemos
        * un id>-1 se trata de la modificación de una tarea, se obtendran los datos para mostrarlos.
        */
        if(id != -1){
            tarea =  ((Lilla) getApplication()).tareasBD.getTareaSemanal(id);
            etTituloTareaSemanal.setText(tarea.getTitulo());
            etDescripcionTareaSemanal.setText(tarea.getDescripcion());
            spinnerNivelImportanciaTareaSemanal.setSelection(tarea.getNivel_importancia()-1);
            horaTv.setTimeInMillis(tarea.getHora());
            twvHoraTareaSemanal.setText(String.format("%02d", horaTv.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", horaTv.get(Calendar.MINUTE)));
            toggleButtonLunes.setChecked(tarea.isLunes());
            if(toggleButtonLunes.isChecked())toggleButtonLunes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            toggleButtonMartes.setChecked(tarea.isMartes());
            if(toggleButtonMartes.isChecked())toggleButtonMartes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            toggleButtonMiercoles.setChecked(tarea.isMiercoles());
            if(toggleButtonMiercoles.isChecked())toggleButtonMiercoles.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            toggleButtonJueves.setChecked(tarea.isJueves());
            if(toggleButtonJueves.isChecked())toggleButtonJueves.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            toggleButtonViernes.setChecked(tarea.isViernes());
            if(toggleButtonViernes.isChecked())toggleButtonViernes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            toggleButtonSabado.setChecked(tarea.isSabado());
            if(toggleButtonSabado.isChecked())toggleButtonSabado.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            toggleButtonDomingo.setChecked(tarea.isDomingo());
            if(toggleButtonDomingo.isChecked())toggleButtonDomingo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        }else{
            horaTv.setTimeInMillis(System.currentTimeMillis());
            twvHoraTareaSemanal.setText(String.format("%02d", horaTv.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", horaTv.get(Calendar.MINUTE)));
        }



        /*listener textView de hora para iniciar TimePickerDialog para hora de tarea. */
        twvHoraTareaSemanal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                new TimePickerDialog(EdicionTareaSemanal.this,
                        t,
                        horaTv.get(Calendar.HOUR_OF_DAY),
                        horaTv.get(Calendar.MINUTE),
                        true).show();
            }

        });


        /* Listeners para cuando se pulse toggleButtons se muestre letra de semana con diferente color. */
        toggleButtonLunes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonLunes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonLunes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });

        toggleButtonMartes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonMartes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonMartes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });

        toggleButtonMiercoles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonMiercoles.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonMiercoles.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });

        toggleButtonJueves.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonJueves.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonJueves.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });

        toggleButtonViernes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonViernes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonViernes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });

        toggleButtonSabado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonSabado.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonSabado.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });

        toggleButtonDomingo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButtonDomingo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
                } else {
                    toggleButtonDomingo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                }
            }
        });


        /*listener button para guarda datos de tarea */
        fActionButtonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Se comprueba que el título no este vacío. */
                if(TextUtils.isEmpty(etTituloTareaSemanal.getText().toString().trim())) {
                    etTituloTareaSemanal.setError(getString(R.string.CampoTituloError));
                    return;
                }

                /* Se comprueba que este pulsado al menos un dia de la semana */
                if(!toggleButtonLunes.isChecked() && !toggleButtonMartes.isChecked() &&
                   !toggleButtonMiercoles.isChecked() && !toggleButtonJueves.isChecked() &&
                   !toggleButtonViernes.isChecked() && !toggleButtonSabado.isChecked() &&
                   !toggleButtonDomingo.isChecked()) {
                    Snackbar.make(getCurrentFocus(), getString(R.string.SeleccionDias), Snackbar.LENGTH_SHORT)
                            .setDuration(4000)
                            .show();
                    return;
                }



                horaTv.set(Calendar.DAY_OF_MONTH,1);
                horaTv.set(Calendar.MONTH, Calendar.JANUARY);
                horaTv.set(Calendar.YEAR, 1970);
                horaTv.set(Calendar.SECOND,0);
                horaTv.set(Calendar.MILLISECOND,0);


                tarea =  new TareaSemanal(
                        id,
                        etTituloTareaSemanal.getText().toString().trim(),
                        etDescripcionTareaSemanal.getText().toString().trim(),
                        (int) (spinnerNivelImportanciaTareaSemanal.getSelectedItemId()),
                        toggleButtonLunes.isChecked(),
                        toggleButtonMartes.isChecked(),
                        toggleButtonMiercoles.isChecked(),
                        toggleButtonJueves.isChecked(),
                        toggleButtonViernes.isChecked(),
                        toggleButtonSabado.isChecked(),
                        toggleButtonDomingo.isChecked(),
                        horaTv.getTimeInMillis(),
                        0
                        );

                /* Generamos el instante de la siguiente alarma  y lo guardamos en la tarea */
                Calendar diaAlarma = FunctionsCalendar.calculoFechaSemanaAlarma(tarea);
                diaAlarma.set(Calendar.HOUR_OF_DAY, horaTv.get(Calendar.HOUR_OF_DAY));
                diaAlarma.set(Calendar.MINUTE, horaTv.get(Calendar.MINUTE));
                diaAlarma.set(Calendar.SECOND, horaTv.get(Calendar.SECOND));
                diaAlarma.set(Calendar.MILLISECOND, horaTv.get(Calendar.MILLISECOND));
                tarea.setFechaHoraSigAlarma(diaAlarma.getTimeInMillis());

                /*
                * Si obtenemos id=-1 se trata de una nueva tarea, si obtenemos un id>-1 se trata de
                * la modificación de una tarea. Registraremos los datos en la base de datos.
                */
                if(tarea.getId_tarea()==-1){
                    nuevaTarea();
                } else{
                    actualizarTarea();
                }

                /* Se establece alarma */
                FunctionsAlarmManager.establecerAlarmaInternaSemanal(tarea,getApplicationContext());

                /* Actualizamos ReciclerViews de tareas programadas */
                if(ListaTareasProgramadas.tareasAdapter != null ) {
                    ListaTareasProgramadas.tareasAdapter.setList( ((Lilla) getApplication()).tareasBD.getAllTareas());
                    ListaTareasProgramadas.tareasAdapter.notifyDataSetChanged();
                }



                finish();
            }
        });

        fActionButtonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edicion_tarea, menu);
        MenuItem accionBorrar = menu.findItem(R.id.accion_borrar);

        /* Si id!=1 se muestra la opcion de borrar tarea */
        if(id!=-1)
            accionBorrar.setVisible(true);
        else
            accionBorrar.setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_borrar:
                this.borrarTarea();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Borrado de tarea que se esta modificando.
     */
    private void borrarTarea(){
        if(id==-1) {
            Snackbar.make(getCurrentFocus(), getString(R.string.BorradoErroneo), Snackbar.LENGTH_SHORT)
                    .setDuration(4000)
                    .show();
            return;
        }else {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.titulo_borrar_tarea))
                    .setMessage(getResources().getString(R.string.mensaje_borrar_tarea))
                    .setPositiveButton(getResources().getString(R.string.confirmar_alert_dialog), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ((Lilla) getApplication()).tareasBD.borrarTarea(id);
                            id=-1;
                            /* Actualizamos ReciclerViews de tareas programadas */
                            if(ListaTareasProgramadas.tareasAdapter != null ) {
                                ListaTareasProgramadas.tareasAdapter.setList(((Lilla) getApplication()).tareasBD.getAllTareas());
                                ListaTareasProgramadas.tareasAdapter.notifyDataSetChanged();
                            }
                            finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancelar_alert_dialog), null)
                    .show();
        }
    }

    /**
     * Se registra en base de datos nueva tarea puntual introducida.
     * @return id identificador de nueva tarea.
     */
    private int nuevaTarea(){
        tarea.setId_tarea( ((Lilla) getApplication()).tareasBD.nuevaTareaSemanal(tarea));
        return tarea.getId_tarea();
    }

    /** Se actualiza en base de datos la tarea puntual modificada. **/
    private void actualizarTarea(){
        ((Lilla) getApplication()).tareasBD.actualizaTareaSemanal(tarea);
    }

    /**
     * Si se esta modificando una tarea, ante onPause se restablece la alarma con datos anteriores.
     */
    @Override
    public void onPause() {
        super.onPause();
        if(id != -1) {
            FunctionsAlarmManager.establecerAlarmaInternaSemanal(tarea, getApplicationContext());
        }
    }

    /**
     * Si se esta modificando una tarea, ante onResume se elimina la alarma para que no salte alarma
     * mientras se edita la tarea.
     */
    @Override
    public void onResume() {
        super.onResume();
        if(id != -1) {
            FunctionsAlarmManager.borrarAlarmaInternaSemanal(tarea,getApplicationContext());
        }
    }
}


