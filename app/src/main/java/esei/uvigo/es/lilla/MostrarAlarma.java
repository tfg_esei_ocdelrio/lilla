package esei.uvigo.es.lilla;

import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Oscar Campos del Río on 12/02/2017.
 */

/**
 * Activity de mostrado de alarma.
 */
public class MostrarAlarma extends AppCompatActivity {
    private Ringtone ringtone;
    private TextView tvTituloAlarma, tvHoraAlarma;
    private LinearLayout llMostrarAlarma;
    private FloatingActionButton fbApagarAlarma;
    private Vibrator vibrator;
    private final long[] pattern = {0, 1000, 500};
    private AudioManager mAudioManager;
    private int volumeAlarmn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle params = intent.getExtras();

        /*
         * Declaración de PowerManager generando Wake Lock, mecanimos para indicar que la aplicación
         * necesita que el dispositivo este activo.
         */
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wl=pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.PARTIAL_WAKE_LOCK, "Lilla");
        wl.acquire();

        /*
         * Añadimos flags a clase abstracta Windows para cuando se lance la activity se pueda mostrar
         * tanto con el movil bloqueado y pantalla apagada encendiendo la pantalla y manteniendola
         * encendida mientras se muestre la activity.
         */
        final Window win= getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_mostrar_alarma);
        fbApagarAlarma = (FloatingActionButton) findViewById(R.id.fb_apagar_alarma);
        tvTituloAlarma = (TextView) findViewById(R.id.tv_titulo_alarma);
        tvHoraAlarma =  (TextView) findViewById(R.id.tv_hora_alarma);
        llMostrarAlarma = (LinearLayout) findViewById(R.id.ll_mostrar_alarma);



        vibrator = (Vibrator) getSystemService(getApplicationContext().VIBRATOR_SERVICE);

        /*
         * Obtenemos URI de sonido de alarma por defecto configurado en el terminal.
         */
        Uri defaultAlarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), defaultAlarm);

        /*
         * Configuramos en el terminal la alarma a maximo volumen para asegurarnos que
         * suene la alarma aunque tenga el volumen previamente bajo
         */
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        volumeAlarmn = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);
        mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, maxVolume, AudioManager.FLAG_PLAY_SOUND);


        if(Build.VERSION.SDK_INT >= 21) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            ringtone.setAudioAttributes(audioAttributes);
        }
        else{
            ringtone.setStreamType(AudioManager.STREAM_ALARM);
        }


        tvTituloAlarma.setText(params.getString("titulo"));
        Calendar hora = Calendar.getInstance(TimeZone.getDefault());
        hora.setTimeInMillis(params.getLong("hora",0));
        tvHoraAlarma.setText(String.format("%02d", hora.get(Calendar.HOUR_OF_DAY)) +
                ":" + String.format("%02d", hora.get(Calendar.MINUTE)));
        llMostrarAlarma.setBackgroundColor(Color.parseColor("#" + params.getString("color")));

        fbApagarAlarma.setOnClickListener(new View.OnClickListener() {

            /**
             * En caso de pulsar el floatingbutton  detendremos la vibración y el sonido de alarma, reconfigurando
             * el volumen del alarma a los valores anteriores.
             */
            @Override
            public void onClick(View v){
                stopVibrator();
                mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, volumeAlarmn, AudioManager.FLAG_PLAY_SOUND);
                ringtone.stop();
                finish();
            }
        });

        wl.release();
    }


    void stopVibrator (){
        vibrator.cancel();
    }

    /**
     * Al salir de la activity detendremos la vibración y el sonido de alarma, reconfigurando
     * el volumen del alarma a los valores anteriores.
     */
    @Override
    public void onPause() {
        super.onPause();
        stopVibrator();
        mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, volumeAlarmn, AudioManager.FLAG_PLAY_SOUND);
        ringtone.stop();
    }


    /**
     * Comienza a sonar alarma y vibra el dispositivo.
     */
    @Override
    public void onResume(){
        super.onResume();
        vibrator.vibrate(pattern, 0);
        ringtone.play();
    }
}
