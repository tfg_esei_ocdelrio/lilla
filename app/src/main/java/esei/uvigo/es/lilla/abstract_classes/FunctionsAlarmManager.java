package esei.uvigo.es.lilla.abstract_classes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import esei.uvigo.es.lilla.entities.TareaNotificada;
import esei.uvigo.es.lilla.entities.TareaPuntual;
import esei.uvigo.es.lilla.entities.TareaSemanal;
import esei.uvigo.es.lilla.receivers.ReceiverAlarmTareaPuntual;
import esei.uvigo.es.lilla.receivers.ReceiverAlarmTareaSemanal;
import esei.uvigo.es.lilla.receivers.ReceiverNotificationTareaConfirmada;
import esei.uvigo.es.lilla.receivers.ReceiverNotificationTareaSinConfirmar;
import esei.uvigo.es.lilla.receivers.ReintentoEnvioCorreo;

/**
 * Created by Oscar Campos del Río on 15/02/2017.
 */

/**
 * Clase abstracta con funciones para establecer y borrar alarmas en el AlarmManager.
 */
public abstract class FunctionsAlarmManager {

    /**
     * Establecer alarma en AlarmManager de una tarea semanal.
     * @param tarea Tarea semanal que estableceremos la alarma.
     * @param context contexto pasado de la activity.
     */
    static public void establecerAlarmaInternaSemanal(TareaSemanal tarea, Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
               Intent intent  = new Intent(context, ReceiverAlarmTareaSemanal.class);
        long setTime = tarea.getFechaHoraSigAlarma();
        intent.putExtra("fechaHora",setTime);
        intent.putExtra("id",tarea.getId_tarea());
        PendingIntent pIntent = PendingIntent.getBroadcast(context, tarea.getId_tarea(), intent, PendingIntent.FLAG_CANCEL_CURRENT);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(setTime, pIntent);
            manager.setAlarmClock(alarmClockInfo, pIntent);

        } else  if(Build.VERSION.SDK_INT >= 19)
            manager.setExact(AlarmManager.RTC_WAKEUP, setTime, pIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, setTime, pIntent);
    }

    /**
     * Borrar alarma en AlarmManager de una tarea semanal.
     * @param tarea Tarea semanal que borraremos la alarma anteriormente establecida.
     * @param context contexto pasado del activity.
     */
    static public void borrarAlarmaInternaSemanal(TareaSemanal tarea, Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
       // Calendar diaAlarma = FunctionsCalendar.calculoFechaSemanaAlarma(tarea);
        Intent intent  = new Intent(context, ReceiverAlarmTareaSemanal.class);
        long setTime = tarea.getFechaHoraSigAlarma();
        intent.putExtra("fechaHora",setTime);
        intent.putExtra("id",tarea.getId_tarea());
        PendingIntent pIntent = PendingIntent.getBroadcast(context, tarea.getId_tarea(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pIntent);
    }

    /**
     * Establecer alarma en AlarmManager de una tarea puntual..
     * @param tarea Tarea puntual que estableceremos la alarma.
     * @param context contexto pasado del activity.
     */
    static public void establecerAlarmaInternaPuntual(TareaPuntual tarea, Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent  = new Intent(context, ReceiverAlarmTareaPuntual.class);
        long setTime = (tarea.getFechaHora());
        intent.putExtra("fechaHora",setTime);
        intent.putExtra("id",tarea.getId_tarea());
        PendingIntent pIntent = PendingIntent.getBroadcast(context, tarea.getId_tarea(), intent, PendingIntent.FLAG_CANCEL_CURRENT);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(setTime, pIntent);
            manager.setAlarmClock(alarmClockInfo, pIntent);

        } else  if(Build.VERSION.SDK_INT >= 19)
            manager.setExact(AlarmManager.RTC_WAKEUP, setTime, pIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, setTime, pIntent);
    }

    /**
     * Borrar alarma en AlarmManager de una tarea puntual.
     * @param tarea Tarea puntual que borraremos la alarma anteriormente establecida.
     * @param context contexto pasado del activity.
     */
    static public void borrarAlarmaInternaPuntual(TareaPuntual tarea, Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent  = new Intent(context, ReceiverAlarmTareaPuntual.class);
        long setTime = (tarea.getFechaHora());
        intent.putExtra("fechaHora",setTime);
        intent.putExtra("id",tarea.getId_tarea());
        PendingIntent pIntent = PendingIntent.getBroadcast(context, tarea.getId_tarea(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pIntent);
    }


    /**
     * Establecer alarma en AlarmManager de las notificaciones de una tarea notificada sin confirmar.
     * @param tareaNotificada Tarea notificada que estableceremos la alarma  para las notificaciones.
     * @param minutos minutos para configurar alarma de la notificación.
     * @param limiteNotificaciones cantidad limite de notificaciones de las notificaciones.
     * @param avisoCP indicador de si se notificada al cuidador principal (true=si, false=no).
     * @param context contexto pasado del activity.
     */
    static public void establecerNotificacionTareaSinConfirmar(TareaNotificada tareaNotificada, int minutos, int limiteNotificaciones, boolean avisoCP, Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReceiverNotificationTareaSinConfirmar.class);
        intent.putExtra("id",tareaNotificada.getId_tarea_notificada());
        intent.putExtra("titulo",tareaNotificada.getTitulo());
        intent.putExtra("hora",tareaNotificada.getFechaHoraTarea());
        intent.putExtra("fechaHoraNotificacion",tareaNotificada.getFechaHoraNotificacion());
        intent.putExtra("minutos",minutos);
        intent.putExtra("avisoCP",avisoCP);
        intent.putExtra("limiteNotificaciones",limiteNotificaciones);

        PendingIntent pIntent = PendingIntent.getBroadcast(context,  tareaNotificada.getId_tarea_notificada(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        long setTime = tareaNotificada.getFechaHoraNotificacion();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(setTime, pIntent);
            manager.setAlarmClock(alarmClockInfo, pIntent);

        } else  if(Build.VERSION.SDK_INT >= 19)
            manager.setExact(AlarmManager.RTC_WAKEUP, setTime, pIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, setTime, pIntent);
    }

    /**
     * Borra alarma en AlarmManager de las notificaciones de una tarea notificada.
     * @param tareaNotificada Tarea notificada que estableceremos la alarma  para las notificaciones.
     * @param minutos minutos para configurar alarma de la notificación.
     * @param limiteNotificaciones cantidad limite de notificaciones de las notificaciones.
     * @param avisoCP indicador de si se notificada al cuidador principal (true=si, false=no).
     * @param context contexto pasado del activity.
     */
    static public void borrarNotificacionTareaSinConfirmar(TareaNotificada tareaNotificada, int minutos, int limiteNotificaciones, boolean avisoCP, Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReceiverNotificationTareaSinConfirmar.class);
        intent.putExtra("id", tareaNotificada.getId_tarea_notificada());
        intent.putExtra("titulo", tareaNotificada.getTitulo());
        intent.putExtra("hora", tareaNotificada.getFechaHoraTarea());
        intent.putExtra("fechaHoraNotificacion",tareaNotificada.getFechaHoraNotificacion());
        intent.putExtra("minutos",minutos);
        intent.putExtra("avisoCP", avisoCP);
        intent.putExtra("limiteNotificaciones", limiteNotificaciones);

        PendingIntent mAlarmSender = PendingIntent.getBroadcast(context, tareaNotificada.getId_tarea_notificada(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(mAlarmSender);
    }


    /**
     * Establecer alarma en AlarmManager de las notificaciones de una tarea notificada confirmada.
     * @param tareaNotificada Tarea notificada que estableceremos la alarma  para las notificaciones.
     * @param minutos minutos para configurar alarma de la notificación.
     * @param limiteNotificaciones cantidad limite de notificaciones de las notificaciones.
     * @param context contexto pasado del activity.
     */
    static public void establecerNotificacionTareaConfirmada(TareaNotificada tareaNotificada, int minutos,  int limiteNotificaciones,  Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReceiverNotificationTareaConfirmada.class);
        intent.putExtra("id",tareaNotificada.getId_tarea_notificada());
        intent.putExtra("titulo",tareaNotificada.getTitulo());
        intent.putExtra("hora",tareaNotificada.getFechaHoraTarea());
        intent.putExtra("fechaHoraNotificacion",tareaNotificada.getFechaHoraNotificacion());
        intent.putExtra("minutos",minutos);
        intent.putExtra("limiteNotificaciones",limiteNotificaciones);

        PendingIntent pIntent = PendingIntent.getBroadcast(context,  tareaNotificada.getId_tarea_notificada(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        long setTime = tareaNotificada.getFechaHoraNotificacion();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(setTime, pIntent);
            manager.setAlarmClock(alarmClockInfo, pIntent);

        } else  if(Build.VERSION.SDK_INT >= 19)
            manager.setExact(AlarmManager.RTC_WAKEUP, setTime, pIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, setTime, pIntent);
    }

    /**
     * Borrar alarma en AlarmManager de las notificaciones de una tarea notificada confirmada.
     * @param tareaNotificada Tarea notificada que estableceremos la alarma  para las notificaciones.
     * @param minutos minutos para configurar alarma de la notificación.
     * @param limiteNotificaciones cantidad limite de notificaciones de las notificaciones.
     * @param context contexto pasado del activity.
     */
    static public void borrarNotificacionTareaConfirmada(TareaNotificada tareaNotificada, int minutos,  int limiteNotificaciones,  Context context){
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReceiverNotificationTareaConfirmada.class);
        intent.putExtra("id",tareaNotificada.getId_tarea_notificada());
        intent.putExtra("titulo",tareaNotificada.getTitulo());
        intent.putExtra("hora",tareaNotificada.getFechaHoraTarea());
        intent.putExtra("fechaHoraNotificacion",tareaNotificada.getFechaHoraNotificacion());
        intent.putExtra("minutos",minutos);
        intent.putExtra("limiteNotificaciones",limiteNotificaciones);

        PendingIntent pIntent = PendingIntent.getBroadcast(context,  tareaNotificada.getId_tarea_notificada(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pIntent);
    }


    /**
     * Generación de alarma en AlarmaManager para reintento de envio de correo al cuicador princiapal.
     * @param idTaraea identificación de la tarea a notificar.
     * @param context contexto pasado del activity.
     */
    static public void establecerAlarmaReintentoEnvioCorreo(int idTaraea, Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReintentoEnvioCorreo.class);
        intent.putExtra("id",idTaraea);

        PendingIntent pIntent = PendingIntent.getBroadcast(context,  idTaraea, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        long setTime = System.currentTimeMillis()+60000;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(setTime, pIntent);
            manager.setAlarmClock(alarmClockInfo, pIntent);

        } else  if(Build.VERSION.SDK_INT >= 19)
            manager.setExact(AlarmManager.RTC_WAKEUP, setTime, pIntent);
        else
            manager.set(AlarmManager.RTC_WAKEUP, setTime, pIntent);
    }

    /**
     * Borrar  alarma en AlarmaManager para reintento de envio de correo al cuicador princiapal.
     * @param idTaraea identificación de la tarea a notificar.
     * @param context contexto pasado del activity.
     */
    static public void borrarAlarmaReintentoEnvioCorreo(int idTaraea, Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ReintentoEnvioCorreo.class);
        intent.putExtra("id",idTaraea);
        PendingIntent pIntent = PendingIntent.getBroadcast(context,  idTaraea, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pIntent);

    }




}
