package esei.uvigo.es.lilla.adapters;

import android.content.Context;
import java.util.List;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;

/**
 * Created by Oscar Campos del Río on 07/02/2017.
 */

/**
 * Clase adapter de las tareas notificadas, para gestionar registros entre base de datos y
 * AdapterTareasNotificadas para mostrar registros en ReciclerViews.
 */
public class AdapterTareasNotificadasBD extends AdapterTareasNotificadas {
    protected List<TareaNotificada> listaTareas;

    public AdapterTareasNotificadasBD(Context contexto, List<TareaNotificada> listaTareas) {
        super(contexto);
        this.listaTareas = listaTareas;
    }

    public List<TareaNotificada> getListaTareas() {
        return listaTareas;
    }

    public void setList(List<TareaNotificada> listaTareas) {
        this.listaTareas = listaTareas;
    }

    public TareaNotificada lugarPosicion(int posicion) {
        return listaTareas.get(posicion);
    }

    public int idPosicion(int posicion) {
        return listaTareas.get(posicion).getId_tarea_notificada();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TareaNotificada tarea = lugarPosicion(position);
        NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) contexto.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
        personalizaVista(holder, tarea, nivelImportanciaTarea);
    }

    @Override
    public int getItemCount() {
        return listaTareas.size();
    }

}
