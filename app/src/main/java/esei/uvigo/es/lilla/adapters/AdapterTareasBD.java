package esei.uvigo.es.lilla.adapters;

import android.content.Context;
import java.util.List;
import esei.uvigo.es.lilla.entities.Tarea;

/**
 * Created by Oscar Campos del Río on 01/02/2017.
 */

/**
 * Clase adapter de las tareas configuradas, para gestionar registros entre base de datos y
 * AdapterTareas para mostrar registros en ReciclerViews.
 */
public class  AdapterTareasBD extends AdapterTareas {
    protected List<Tarea> listaTareas;

    public AdapterTareasBD(Context contexto, List<Tarea> listaTareas) {
        super(contexto);
        this.listaTareas = listaTareas;
    }

    public List<Tarea> getListaTareas() {
        return listaTareas;
    }

    public void setList(List<Tarea> listaTareas) {
        this.listaTareas = listaTareas;
    }

    public Tarea lugarPosicion(int posicion) {
        return listaTareas.get(posicion);
    }

    public int idPosicion(int posicion) {
       return listaTareas.get(posicion).getId_tarea();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Tarea tarea = lugarPosicion(position);
        personalizaVista(holder, tarea);
    }

    @Override
    public int getItemCount() {
        return listaTareas.size();
    }
}

