package esei.uvigo.es.lilla.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import esei.uvigo.es.lilla.services.LillaBootService;

/**
 * Created by Oscar Campos del Río on 01/03/2017.
 */

/**
 * Receiver para lanzar servicio de inicio del termina.
 */
public class ReceiverLillaBoot extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, LillaBootService.class);
            context.startService(serviceIntent);
        }

    }

}
