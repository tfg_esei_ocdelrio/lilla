package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Rio 06/03/2017.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Actividad de configuración de APP
 */
public class PreferenciasActivity extends AppCompatActivity {
    static final int REQUEST_GMAILSEND = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PreferenciasFragment(),"PreferenciasFragment").commit();

    }

    /**
     * Resultado de llamada a activity GmailSend
     * @param requestCode, se contempla solo REQUEST_GMAILSEND
     * @param resultCode RESULT_OK: se ha configuado correctamente el api GMAIL. RESULT_CANCELED: se ha producido un error en la configuración.
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GMAILSEND) {
            if (resultCode == RESULT_OK) {
                PreferenciasFragment frag = ((PreferenciasFragment) getFragmentManager().findFragmentByTag("PreferenciasFragment"));
                frag.correoNotificacionCp.setSelectable(false);

            } else if (resultCode ==  RESULT_CANCELED){
                PreferenciasFragment frag = ((PreferenciasFragment) getFragmentManager().findFragmentByTag("PreferenciasFragment"));
                frag.notificacionCpEmail.setChecked(false);
                frag.correoNotificacionCp.setSelectable(true);

            }
        }
    }

    /**
     *
     * @param intentEmail Intent con datos "titulo" y "cuerpo" del correo.
     */
    public void envioCorreoEleccionCP(Intent intentEmail){
        startActivityForResult(intentEmail, PreferenciasActivity.REQUEST_GMAILSEND);
    }



}
