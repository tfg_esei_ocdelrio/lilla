package esei.uvigo.es.lilla.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.view.WindowManager;

import java.util.Calendar;
import java.util.TimeZone;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.MainActivity;
import esei.uvigo.es.lilla.MostrarAlarma;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.abstract_classes.FunctionsCalendar;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;
import esei.uvigo.es.lilla.entities.TareaSemanal;

/**
 * Created by Oscar Campos del Río on 15/02/2017.
 */

/**
 * Receiver alarma establecida en AlarmManager para mostrar alarma de tarea semanal.
 */
public class ReceiverAlarmTareaSemanal extends WakefulBroadcastReceiver {
    private TareaNotificada tareaNotificada;
    private TareaSemanal tarea;
    private NivelImportanciaTarea nivelImportanciaTarea;
    private int idTareaNotificada;

    @Override
    public void onReceive(Context context, android.content.Intent i) {
        Calendar fechaHoraNotificacion = Calendar.getInstance(TimeZone.getDefault());
        Bundle params = i.getExtras();
        Calendar fechaHoraAlarma = Calendar.getInstance(TimeZone.getDefault());
        fechaHoraAlarma.setTimeInMillis(params.getLong("fechaHora"));

        /* Obtenemos tarea y nivel de importancia. */
        tarea = ((Lilla) context.getApplicationContext()).tareasBD.getTareaSemanal(params.getInt("id"));
        nivelImportanciaTarea = ((Lilla) context.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());

        /* Se añade a la base de datos la tarea notificada */
        fechaHoraNotificacion.add(Calendar.MINUTE, nivelImportanciaTarea.getMinutos());
        tareaNotificada = new TareaNotificada(tarea.getId_tarea(),tarea.getTitulo(),tarea.getDescripcion(),
                                                fechaHoraAlarma.getTimeInMillis(),0L,fechaHoraNotificacion.getTimeInMillis(),
                                                tarea.getNivel_importancia(),0,false);
        idTareaNotificada = ((Lilla) context.getApplicationContext()).tareasBD.nuevaTareaNotificada(tareaNotificada);
        tareaNotificada.setId_tarea_notificada(idTareaNotificada);


        /* Si estan instanciados ReciclersViews actualizamos contenido. */
        if (MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter != null) {
            MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.setList(((Lilla) context.getApplicationContext()).tareasBD.getAllTareasSinConfirmar());
            MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.notifyDataSetChanged();
        }

        /* Añadimos siguiente alarma de la tarea semanal.*/
        Calendar diaAlarma = FunctionsCalendar.calculoFechaSemanaAlarma(tarea);
        Calendar hora = Calendar.getInstance(TimeZone.getDefault());
        hora.setTimeInMillis(tarea.getHora());
        diaAlarma.set(Calendar.HOUR_OF_DAY,hora.get(Calendar.HOUR_OF_DAY));
        diaAlarma.set(Calendar.MINUTE,hora.get(Calendar.MINUTE));
        diaAlarma.set(Calendar.SECOND,hora.get(Calendar.SECOND));
        diaAlarma.set(Calendar.MILLISECOND,hora.get(Calendar.MILLISECOND));
        tarea.setFechaHoraSigAlarma(diaAlarma.getTimeInMillis());
        ((Lilla) context.getApplicationContext()).tareasBD.actualizaTareaSemanal(tarea);
        FunctionsAlarmManager.establecerAlarmaInternaSemanal(tarea, context);

        /* Generamos alarma para notificaciones de la tarea semanal. */
        FunctionsAlarmManager.establecerNotificacionTareaSinConfirmar(tareaNotificada, nivelImportanciaTarea.getMinutos(),
                nivelImportanciaTarea.getLimite_notificaciones(),
                nivelImportanciaTarea.isAviso_cp(), context);


        /* Preparamos intent para lanzar activity y mostrar alarma de la tarea. */
        Intent alarmIntent = new Intent(context, MostrarAlarma.class);
        alarmIntent.putExtra("id", tarea.getId_tarea());
        alarmIntent.putExtra("titulo", tarea.getTitulo());
        alarmIntent.putExtra("hora", tarea.getHora());
        alarmIntent.putExtra("color", nivelImportanciaTarea.getColor());
        alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK + Intent.FLAG_ACTIVITY_NO_HISTORY);
        alarmIntent.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED +
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD +
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON +
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        context.startActivity(alarmIntent);



    }
}
