package esei.uvigo.es.lilla.entities;


/**
 * Created by Oscar Campos del Rio on 25/01/2017.
 */

/**
 * Clase extendida de tarea que representa una tarea con repetición durante los días de las semana.
 */
public class TareaSemanal extends Tarea {
    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los lunes(true=si, false=no).
     */
    private boolean lunes;

    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los martes(true=si,
     * false=no).
     */
    private boolean martes;

    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los miercoles(true=si,
     * false=no).
     */
    private boolean miercoles;

    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los jueves(true=si,
     * false=no).
     */
    private boolean jueves;

    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los viernes(true=si,
     * false=no).
     */
    private boolean viernes;

    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los sabado(true=si,
     * false=no).
     */
    private boolean sabado;

    /**
     * Atributo boleano que indica que la tarea se notifica por alarma los domingo(true=si,
     * false=no).
     */
    private boolean domingo;

    /**
     * Atributo representa hora en que debe ser realizada la tarea. Usado para ejecución de la alarma
     * (valor en milisegundos desde 01/01/1970, solo se recoge los milisegundos para
     * representar la hora).
     */
    private long hora;

    /**
     * Atributo representa fecha y hora de siguiente alarma de la tarea
     * (valor en milisegundos desde 01/01/1970. Cada vez que se ejecute la alarma, este atributo
     * se reasigna cuando la app estña en ejecución con la llamada a la función
     * @see esei.uvigo.es.lilla.abstract_classes.FunctionsCalendar#calculoFechaSemanaAlarma )
     */
    private long fechaHoraSigAlarma;

    public TareaSemanal(int id_tarea, String titulo, String descripcion,
                        int id_nivel,
                        boolean lunes, boolean martes,boolean miercoles, boolean jueves,
                        boolean viernes, boolean sabado, boolean domingo,long hora, long fechaHoraSigAlarma) {
        super(id_tarea, titulo, descripcion, id_nivel);
        this.setLunes(lunes);
        this.setMartes(martes);
        this.setMiercoles(miercoles);
        this.setJueves(jueves);
        this.setViernes(viernes);
        this.setSabado(sabado);
        this.setDomingo(domingo);
        this.setHora(hora);
        this.setFechaHoraSigAlarma(fechaHoraSigAlarma);

    }

    public boolean isLunes() {
        return lunes;
    }

    public void setLunes(boolean lunes) {
        this.lunes = lunes;
    }

    public boolean isMartes() {
        return martes;
    }

    public void setMartes(boolean martes) {
        this.martes = martes;
    }

    public boolean isMiercoles() {
        return miercoles;
    }

    public void setMiercoles(boolean miercoles) {
        this.miercoles = miercoles;
    }

    public boolean isJueves() {
        return jueves;
    }

    public void setJueves(boolean jueves) {
        this.jueves = jueves;
    }

    public boolean isViernes() {
        return viernes;
    }

    public void setViernes(boolean viernes) {
        this.viernes = viernes;
    }

    public boolean isSabado() {
        return sabado;
    }

    public void setSabado(boolean sabado) {
        this.sabado = sabado;
    }

    public boolean isDomingo() {
        return domingo;
    }

    public void setDomingo(boolean domingo) {
        this.domingo = domingo;
    }

    public long getHora() {
        return hora;
    }

    public void setHora(long hora) {
        this.hora = hora;
    }

    public long getFechaHoraSigAlarma() {
        return fechaHoraSigAlarma;
    }

    public void setFechaHoraSigAlarma(long fechaHoraSigAlarma) {
            this.fechaHoraSigAlarma = fechaHoraSigAlarma;
    }

    @Override
    public String toString() {
        return "TareaSemanal{" +
                "lunes=" + lunes +
                ", martes=" + martes +
                ", miercoles=" + miercoles +
                ", jueves=" + jueves +
                ", viernes=" + viernes +
                ", sabado=" + sabado +
                ", domingo=" + domingo +
                ", hora=" + hora +
                ", fechaHoraSigAlarma=" + fechaHoraSigAlarma +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TareaSemanal other = (TareaSemanal) obj;
        if (this.getId_tarea() != other.getId_tarea())
            return false;
        return true;
    }
}
