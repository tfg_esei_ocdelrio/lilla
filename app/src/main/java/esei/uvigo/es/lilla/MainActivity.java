package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 12/02/2017.
 * Based in example of @see <a href="https://developer.android.com/training/material/lists-cards.html?hl=es">Developer Android Crear Listas y tarjetas</a>
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import esei.uvigo.es.lilla.adapters.AdapterTareasNotificadasBD;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.services.LillaAlarmsService;

/**
 * Activity principal App con mostrado de tareas notificadas en tabs.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * El {@link android.support.v4.view.PagerAdapter} proporcionara
     * fragments por cada una de las secciones. Usamos un
     * {@link FragmentPagerAdapter} derivado, que mantendra cada
     * fragmento cargado en memoria. Si esto se vuelve muy pesado en memoria, puede
     * ser mejor cambiar a un
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} contendrá el contenido de la seccion
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Lanzamos servicio para recargar de alarms de las tareas y notificaciones.  */
        Intent serviceIntent = new Intent(getApplicationContext(), LillaAlarmsService.class);
        startService(serviceIntent);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        /*
         *  Crea el adaptador que devolverá un fragment para cada una de las secciones
         *  primarias de la actividad
         */
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        /*
         * Configura ViewPager con el adaptador de secciones.
         */
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_tareas_programadas) {
            Intent i = new Intent(MainActivity.this, ListaTareasProgramadas.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {}


    public static class PlaceholderFragment extends Fragment {
        /**
         * El argumento fragment representa el numero de seccion para este fragment
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /*
         * Declaración de RecyclerViews, LayoutManager y AdaptersTareasNotificadasBD donde se cargarán
         * los datos de las tareas confirmadas y sin confirmar.
         */
        private RecyclerView tareasConfirmadasRecyclerView, tareasSinConfirmarRecyclerView ;
        private RecyclerView.LayoutManager tareasConfirmadasLayoutManager, tareasSinConfirmarLayoutManager;
        public static AdapterTareasNotificadasBD tareasConfirmadasAdapter, tareasSinConfirmarAdapter;
        public static TextView tvSinElementosConfirmados, tvSinElementosSinConfirmar;



        public PlaceholderFragment() {}

        /*
         * Devuelve una nueva instancia de este fragment por el numero de sección dado.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView;

            /*
             * Según numero de sección dado se carga el RecuclerView.
             */
            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 0:
                    rootView = inflater.inflate(R.layout.fragment_tareas_sin_confirmar, container, false);
                    tareasSinConfirmarRecyclerView = (RecyclerView) rootView.findViewById(R.id.recicler_view_tareas_sin_confirmar);
                    tvSinElementosSinConfirmar = (TextView) rootView.findViewById(R.id.tv_sin_elementos);
                    tareasSinConfirmarLayoutManager = new LinearLayoutManager(getContext());
                    tareasSinConfirmarAdapter = new AdapterTareasNotificadasBD(getContext(),  ((Lilla)  ((MainActivity)getActivity()).getApplication()).tareasBD.getAllTareasSinConfirmar());
                    tareasSinConfirmarRecyclerView.setAdapter(tareasSinConfirmarAdapter);
                    tareasSinConfirmarRecyclerView.setLayoutManager(tareasSinConfirmarLayoutManager);

                    /* Si no hay elementos mostramos TextView indicandolo. */
                    if(tareasSinConfirmarAdapter.getItemCount()==0)
                        tvSinElementosSinConfirmar.setVisibility(View.VISIBLE);
                    else
                        tvSinElementosSinConfirmar.setVisibility(View.GONE);

                    return rootView;
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_tareas_confirmadas, container, false);
                    tareasConfirmadasRecyclerView = (RecyclerView) rootView.findViewById(R.id.recicler_view_tareas_confirmadas);
                    tvSinElementosConfirmados = (TextView) rootView.findViewById(R.id.tv_sin_elementos);
                    tareasConfirmadasLayoutManager = new LinearLayoutManager(getContext());
                    tareasConfirmadasAdapter = new AdapterTareasNotificadasBD(getContext(),  ((Lilla)  ((MainActivity)getActivity()).getApplication()).tareasBD.getAllTareasConfirmadas());
                    tareasConfirmadasRecyclerView.setAdapter(tareasConfirmadasAdapter);
                    tareasConfirmadasRecyclerView.setLayoutManager(tareasConfirmadasLayoutManager);

                    /* Si no hay elementos mostramos TextView indicandolo. */
                    if(tareasConfirmadasAdapter.getItemCount()==0)
                        tvSinElementosConfirmados.setVisibility(View.VISIBLE);
                    else
                        tvSinElementosConfirmados.setVisibility(View.GONE);
                    return rootView;
            }

            return null;
        }

        @Override
        public void onResume() {
            super.onResume();
            /* Si no hay elementos mostramos TextView indicandolo. */
            if( MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter != null) {
                if(MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.getItemCount()==0)
                    MainActivity.PlaceholderFragment.tvSinElementosSinConfirmar.setVisibility(View.VISIBLE);
                else
                    MainActivity.PlaceholderFragment.tvSinElementosSinConfirmar.setVisibility(View.GONE);
                }
            /* Si no hay elementos mostramos TextView indicandolo. */
            if(MainActivity.PlaceholderFragment.tareasConfirmadasAdapter != null  ) {
                if (MainActivity.PlaceholderFragment.tareasConfirmadasAdapter.getItemCount() == 0)
                    MainActivity.PlaceholderFragment.tvSinElementosConfirmados.setVisibility(View.VISIBLE);
                else
                    MainActivity.PlaceholderFragment.tvSinElementosConfirmados.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Un {@link FragmentPagerAdapter} que devuelve  fragment correspondiente a una de las tabs.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * getItem se llama para instanciar el fragment de la pagina dada.
         * @param position tabs seleccionad0.
         * @return PlaceholderFragment definida como clase interna estática a continuacion.
         */
        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.tab_sin_realizar);
                case 1:
                    return getResources().getString(R.string.tab_hechas);
            }
            return null;
        }


    }




}
