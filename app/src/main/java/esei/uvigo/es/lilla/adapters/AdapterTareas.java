package esei.uvigo.es.lilla.adapters;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import esei.uvigo.es.lilla.EdicionTareaPuntual;
import esei.uvigo.es.lilla.EdicionTareaSemanal;
import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.R;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.Tarea;
import esei.uvigo.es.lilla.entities.TareaPuntual;
import esei.uvigo.es.lilla.entities.TareaSemanal;

/**
 * Created by Oscar Campos del Río on 31/01/2017.
 */

/**
 * Clase extendida de ReciclerView para el mostrado de las tares programadas.
 */
public class AdapterTareas extends RecyclerView.Adapter<AdapterTareas.ViewHolder> {

    protected Context contexto;
    protected LayoutInflater inflador;   //Crea Layouts a partir del XML
    protected View.OnClickListener onClickListener;

    public AdapterTareas(Context contexto) {
        this.contexto = contexto;
        inflador = (LayoutInflater) contexto
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /*Creamos ViewHolder, con los tipos de elementos a modificar. */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_card_titulo_tarea, tv_card_hora_tarea,
                tv_card_tipo_tarea_info, tv_card_descripcion_tarea, tv_card_tipo_tarea, tv_card_id_tarea;
        public CardView card_tarea;
        public ImageView iv_card_importancia;

        public ViewHolder(View itemView, Context contexto) {
            super(itemView);
            final Context contextoV=contexto;
            tv_card_titulo_tarea = (TextView) itemView.findViewById(R.id.tv_card_titulo_tarea);
            tv_card_hora_tarea = (TextView) itemView.findViewById(R.id.tv_card_hora_tarea);
            tv_card_tipo_tarea_info = (TextView) itemView.findViewById(R.id.tw_card_tipo_tarea_info);
            tv_card_tipo_tarea = (TextView) itemView.findViewById(R.id.tw_card_tipo_tarea);
            tv_card_descripcion_tarea = (TextView) itemView.findViewById(R.id.tv_card_descripcion_tarea);
            tv_card_id_tarea = (TextView) itemView.findViewById(R.id.tv_card_id_tarea);
            iv_card_importancia = (ImageView) itemView.findViewById(R.id.iv_card_importancia);
            card_tarea = (CardView) itemView.findViewById(R.id.card_tarea);

            /* Si pulsamos sobre una card que representa una tarea se verifica a que tipo subclase
             * pertenece para lanzar el propio activity de edición. */
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Intent i;
                    switch (tv_card_tipo_tarea.getText().toString()){
                        case "esei.uvigo.es.lilla.entities.TareaSemanal":
                            i = new Intent(contextoV, EdicionTareaSemanal.class);
                            i.putExtra("_id", Integer.valueOf(tv_card_id_tarea.getText().toString()));
                            contextoV.startActivity(i);
                            break;
                        case "esei.uvigo.es.lilla.entities.TareaPuntual":
                            i = new Intent(contextoV, EdicionTareaPuntual.class);
                            i.putExtra("_id", Integer.valueOf(tv_card_id_tarea.getText().toString()));
                            contextoV.startActivity(i);
                            break;
                    }
                }
            });

        }
    }

    /**
     * Creamos el ViewHolder con la vista de un elemento sin personalizar
     * @param parent
     * @param viewType
     * @return Viewholder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflamos la vista desde el xml
        View v = inflador.inflate(R.layout.elemento_tarea_programada_lista, parent, false);
        v.setOnClickListener(onClickListener);
        return new ViewHolder(v,contexto);
    }

    /**
     * Usando como base el ViewHolder y lo personalizamos
     * @param holder
     * @param posicion
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        Tarea tarea = ((Lilla) contexto.getApplicationContext()).tareasBD.getTareaConfigurada(posicion);
        personalizaVista(holder, tarea);
    }

    /**
     * Personalización del ViewHolder
     * @param holder
     * @param tarea
     */
    public void personalizaVista(ViewHolder holder, Tarea tarea) {
        NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) contexto.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
        holder.tv_card_id_tarea.setText(String.valueOf(tarea.getId_tarea()));
        holder.tv_card_titulo_tarea.setText(tarea.getTitulo());
        holder.tv_card_descripcion_tarea.setText(tarea.getDescripcion());
        holder.card_tarea.setCardBackgroundColor(Color.parseColor("#" + nivelImportanciaTarea.getColor()));

        Calendar hora;

        /* Si el nivel importancia indica que se avisaría al cuidador principal, mostramos icono de
         * importancia. */
        if(nivelImportanciaTarea.isAviso_cp())
            holder.iv_card_importancia.setVisibility(View.VISIBLE);
        else
            holder.iv_card_importancia.setVisibility(View.INVISIBLE);



        /* Segun tipo de tarea se muestra informafición diferente sobre ella
        * TareaSemanaL: dias de la semana a sonar alarma de la tarea.
        * TareaPuntual: dias concreto que sonará la alarma de la tarea. */
        String textTipoTarea = "";
        switch (tarea.getClass().getName()){
            case "esei.uvigo.es.lilla.entities.TareaSemanal":
                TareaSemanal tareaSemanal = (TareaSemanal) tarea;

                hora =  Calendar.getInstance(TimeZone.getDefault());
                hora.setTimeInMillis(((TareaSemanal) tarea).getHora());
                holder.tv_card_hora_tarea.setText(String.format("%02d", hora.get(Calendar.HOUR_OF_DAY)) +
                        ":" + String.format("%02d", hora.get(Calendar.MINUTE)));

                if(tareaSemanal.isLunes()) textTipoTarea= contexto.getResources().getString(R.string.text_lunes) + " " ;
                if(tareaSemanal.isMartes()) textTipoTarea=textTipoTarea + contexto.getResources().getString(R.string.text_martes) + " ";
                if(tareaSemanal.isMiercoles()) textTipoTarea=textTipoTarea + contexto.getResources().getString(R.string.text_miercoles) + " ";
                if(tareaSemanal.isJueves()) textTipoTarea=textTipoTarea + contexto.getResources().getString(R.string.text_jueves) + " ";
                if(tareaSemanal.isViernes()) textTipoTarea=textTipoTarea + contexto.getResources().getString(R.string.text_viernes) + " ";
                if(tareaSemanal.isSabado()) textTipoTarea=textTipoTarea + contexto.getResources().getString(R.string.text_sabado) + " ";
                if(tareaSemanal.isDomingo()) textTipoTarea=textTipoTarea + contexto.getResources().getString(R.string.text_domingo) + " ";
                holder.tv_card_tipo_tarea_info.setText(textTipoTarea);
                holder.tv_card_tipo_tarea.setText("esei.uvigo.es.lilla.entities.TareaSemanal");
                break;
            case "esei.uvigo.es.lilla.entities.TareaPuntual":
                TareaPuntual tareaPuntual = (TareaPuntual) tarea;
                Calendar fecha_hora =  Calendar.getInstance(TimeZone.getDefault());
                fecha_hora.setTimeInMillis(tareaPuntual.getFechaHora());

                holder.tv_card_hora_tarea.setText(String.format("%02d", fecha_hora.get(Calendar.HOUR_OF_DAY)) +
                        ":" + String.format("%02d", fecha_hora.get(Calendar.MINUTE)));

                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
                holder.tv_card_tipo_tarea_info.setText(dateFormatter.format(fecha_hora.getTime()));
                holder.tv_card_tipo_tarea.setText("esei.uvigo.es.lilla.entities.TareaPuntual");
                break;
        }
    }

    /**
     * Retornamos el número de elementos de la lista
     * @return tamaño de lista
     */
    @Override public int getItemCount() {
        return ((Lilla) contexto.getApplicationContext()).tareasBD.tamanyo();
    }
}


