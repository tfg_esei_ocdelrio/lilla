package esei.uvigo.es.lilla.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import esei.uvigo.es.lilla.ListaTareasProgramadas;
import esei.uvigo.es.lilla.MainActivity;


/**
 * Created by Oscar Campos del Río on 26/03/2017.
 */

/**
 * Receiver para reajuste de las tareas que se muestran en las actividades en caso de estar
 * instanciadas.
 */
public class ReceiverTimeZoneChanged extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter != null ){
            MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.notifyDataSetChanged();
        }

        if(MainActivity.PlaceholderFragment.tareasConfirmadasAdapter != null ){
            MainActivity.PlaceholderFragment.tareasConfirmadasAdapter.notifyDataSetChanged();
        }

        if(ListaTareasProgramadas.tareasAdapter != null ) {
            ListaTareasProgramadas.tareasAdapter.notifyDataSetChanged();
        }


    }
}
