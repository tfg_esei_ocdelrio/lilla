package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 12/02/2017.
 */

import android.app.NotificationManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;

/**
 * Activity mostrado de datos de tarea notificada.
 */
public class MostrarTareaNotificada extends AppCompatActivity {
    private int id;
    private TextView tv_card_titulo_tarea, tv_card_hora_tarea,
            tv_card_descripcion_tarea, tv_card_fecha_tarea, tv_card_cantidad_notificaciones,
            tv_card_limite_notificaciones, tv_card_aviso_cp, tv_card_id_tarea, tv_card_hora_fecha_realizada;
    private ImageView iv_card_importancia;
    private TareaNotificada tareaNotificada;
    private NivelImportanciaTarea nivelImportanciaTarea;
    private FloatingActionButton fbConfirmarTarea;
    private RelativeLayout rlMostrarTareaNotificada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_tarea_notificada);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("id",-1);

        /*
        * Si obtenemos id=-1 de valores de intent pasado, no se ha accedido a traves de una tarea
        * por lo que se finaliza la activity.
        */
        if(id==-1){
            finish();
        }

        tv_card_titulo_tarea = (TextView) findViewById(R.id.tv_card_titulo_tarea);
        tv_card_hora_tarea = (TextView) findViewById(R.id.tv_card_hora_tarea);
        tv_card_descripcion_tarea = (TextView) findViewById(R.id.tv_card_descripcion_tarea);
        tv_card_fecha_tarea = (TextView) findViewById(R.id.tv_card_fecha_tarea);
        tv_card_cantidad_notificaciones =  (TextView) findViewById(R.id.tv_card_cantidad_notificaciones);
        tv_card_limite_notificaciones = (TextView) findViewById(R.id.tv_card_limite_notificaciones);
        tv_card_aviso_cp =  (TextView) findViewById(R.id.tv_card_aviso_cp);
        tv_card_id_tarea = (TextView) findViewById(R.id.tv_card_id_tarea);
        tv_card_hora_fecha_realizada = (TextView) findViewById(R.id.tv_card_hora_fecha_realizada);
        iv_card_importancia = (ImageView) findViewById(R.id.iv_card_importancia);
        fbConfirmarTarea = (FloatingActionButton) findViewById(R.id.fb_confirmar_tarea);
        rlMostrarTareaNotificada = (RelativeLayout) findViewById(R.id.activity_mostrar_tarea_notificada);


        /* Obtenemos tarea, su nivel de importancia y los representamos en los views. */
        tareaNotificada = ((Lilla) getApplication()).tareasBD.getTareaNotificada(id);
        nivelImportanciaTarea = ((Lilla) getApplication()).tareasBD.getNivelImportanciaTarea(tareaNotificada.getNivel_importancia());

        tv_card_id_tarea.setText(String.valueOf(tareaNotificada.getId_tarea_notificada()));
        tv_card_titulo_tarea.setText(tareaNotificada.getTitulo());
        Calendar fecha_hora =  Calendar.getInstance(TimeZone.getDefault());
        fecha_hora.setTimeInMillis(tareaNotificada.getFechaHoraTarea());
        tv_card_hora_tarea.setText(String.format("%02d", fecha_hora.get(Calendar.HOUR_OF_DAY)) +
                ":" + String.format("%02d", fecha_hora.get(Calendar.MINUTE)));
        tv_card_descripcion_tarea.setText(tareaNotificada.getDescripcion());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        tv_card_fecha_tarea.setText(dateFormatter.format(fecha_hora.getTime()));
        tv_card_cantidad_notificaciones.setText(tv_card_cantidad_notificaciones.getText().toString() +
                                                " " + tareaNotificada.getCantidad_notificaciones() );
        tv_card_limite_notificaciones.setText(tv_card_limite_notificaciones.getText().toString() +
                                                " " + nivelImportanciaTarea.getLimite_notificaciones());

        /* Mostrado de AvisoCp e icono segun el nivel de importancia tenga a true el atributo. */
        rlMostrarTareaNotificada.setBackgroundColor(Color.parseColor("#" + nivelImportanciaTarea.getColor()));
        if(nivelImportanciaTarea.isAviso_cp()) {
            iv_card_importancia.setVisibility(View.VISIBLE);
            tv_card_aviso_cp.setText(tv_card_aviso_cp.getText().toString() + " " + getResources().getString(R.string.aviso_cp_true));
        }else
            tv_card_aviso_cp.setText(tv_card_aviso_cp.getText().toString() + " " + getResources().getString(R.string.aviso_cp_false));


        /* Mostrado de información en views segun este confirmada la tarea. */
        if(tareaNotificada.isConfirmado()){
            fbConfirmarTarea.setVisibility(View.GONE);
            Calendar fecha_hora_realizado =  Calendar.getInstance(TimeZone.getDefault());
            fecha_hora_realizado.setTimeInMillis(tareaNotificada.getFechaHoraRealizado());
            tv_card_hora_fecha_realizada.setVisibility(View.VISIBLE);
            tv_card_hora_fecha_realizada.setText(getResources().getString(R.string.hora_fecha_realizado) + " " +
                    String.format("%02d", fecha_hora_realizado.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", fecha_hora_realizado.get(Calendar.MINUTE)) + " " +
                    dateFormatter.format(fecha_hora_realizado.getTime())  );

            setTitle(getResources().getString(R.string.tarea_notificada_confirmada_titulo));
        } else {
            tv_card_hora_fecha_realizada.setVisibility(View.GONE);
            fbConfirmarTarea.setVisibility(View.VISIBLE);
            setTitle(getResources().getString(R.string.tarea_notificada_sin_confirmar_titulo));
        }


        /* FloatingActionButton confirmación tarea realizada */
        fbConfirmarTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(MostrarTareaNotificada.this)
                        .setTitle(getResources().getString(R.string.titulo_tarea_confirmada))
                        .setMessage(getResources().getString(R.string.mensaje_tarea_confirmada))
                        .setPositiveButton(getResources().getString(R.string.confirmar_alert_dialog), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                tareaNotificada.setConfirmado(true);
                                tareaNotificada.setCantidad_notificaciones(0);


                                /* Se elimina alarma de futura notificación de tarea sin confirmar. */
                                FunctionsAlarmManager.borrarNotificacionTareaSinConfirmar(tareaNotificada,
                                                                                          nivelImportanciaTarea.getMinutos(),
                                                                                          nivelImportanciaTarea.getLimite_notificaciones(),
                                                                                          nivelImportanciaTarea.isAviso_cp(),MostrarTareaNotificada.this);

                                /* Se elimina alarma de futura notificación de tarea sin confirmar. */
                                FunctionsAlarmManager.borrarAlarmaReintentoEnvioCorreo(tareaNotificada.getId_tarea_notificada(), MostrarTareaNotificada.this);


                                /* Se establece hora de realización y notifificación  */
                                tareaNotificada.setFechaHoraRealizado(System.currentTimeMillis());
                                Calendar fechaHoraNotificacion = Calendar.getInstance(TimeZone.getDefault());
                                fechaHoraNotificacion.setTimeInMillis(System.currentTimeMillis());
                                fechaHoraNotificacion.add(Calendar.MINUTE,nivelImportanciaTarea.getMinutos());
                                tareaNotificada.setFechaHoraNotificacion(fechaHoraNotificacion.getTimeInMillis());

                                /* Se elimina notificacion de tarea en caso de estar siendo mostrada.. */
                                NotificationManager notifManager= (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                                notifManager.cancel(tareaNotificada.getId_tarea_notificada());


                                /* Se establece alarma para notificación como tarea confirmada */
                                FunctionsAlarmManager.establecerNotificacionTareaConfirmada(tareaNotificada,
                                                                                            nivelImportanciaTarea.getMinutos(),
                                                                                            nivelImportanciaTarea.getLimite_notificaciones(),
                                                                                            MostrarTareaNotificada.this);

                                /* Se actualiza tarea en base de datos. */
                                ((Lilla) getApplication()).tareasBD.confirmarTarea(id, tareaNotificada.getFechaHoraRealizado(), tareaNotificada.getFechaHoraNotificacion());


                                /* Actualizamos ReciclerViews de tareas confirmadas y sin confirmar en caso de que estén iniciadas. */
                                if(MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter != null && MainActivity.PlaceholderFragment.tareasConfirmadasAdapter!= null ){
                                    MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.setList( ((Lilla) getApplication()).tareasBD.getAllTareasSinConfirmar());
                                    MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.notifyDataSetChanged();
                                    MainActivity.PlaceholderFragment.tareasConfirmadasAdapter.setList( ((Lilla) getApplication()).tareasBD.getAllTareasConfirmadas());
                                    MainActivity.PlaceholderFragment.tareasConfirmadasAdapter.notifyDataSetChanged();
                                }
                                finish();

                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancelar_alert_dialog), null)
                        .show();
            }
        });

    }
}
