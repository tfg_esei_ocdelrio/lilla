package esei.uvigo.es.lilla.database;

/**
 * Created by Oscar Campos del Río on 25/01/2017.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.Tarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;
import esei.uvigo.es.lilla.entities.TareaPuntual;
import esei.uvigo.es.lilla.entities.TareaSemanal;


/**
 * Clase extentida de SQLiteOpenHelper con metodos y creacion de base de datos de la app.
 */
public class TareasBD extends SQLiteOpenHelper{


    public TareasBD(Context contexto) {
        super(contexto, "LillaBDDEBUG", null, 1);

    }

    @Override
    public void onOpen(SQLiteDatabase bd) {
        super.onOpen(bd);
        if (!bd.isReadOnly()) {
            /* Activamos constraints claves foraneas. */
            bd.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override public void onCreate(SQLiteDatabase bd) {

        /* Creación de tabla NIVELES_IMPORTANCIA referente a la entidad NivelImportanciaTarea.   */
        bd.execSQL("CREATE TABLE NIVELES_IMPORTANCIA_TAREAS (" +
                    "_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "NOMBRE TEXT NOT NULL, " +
                    "LIMITE_NOTIFICACIONES INTEGER NOT NULL DEFAULT 3, " +
                    "AVISO_CP INTEGER NOT NULL DEFAULT 0," +
                    "MINUTOS INTEGER NOT NULL DEFAULT 20," +
                    "COLOR TEXT NOT NULL DEFAULT 'ADD8E6')");

        /* Creación de tabla TAREAS referente a la entidad Tarea.   */
        bd.execSQL("CREATE TABLE TAREAS (" +
                    "_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "TITULO TEXT NOT NULL, " +
                    "DESCRIPCION TEXT, " +
                    "ID_NIVEL INTEGER NOT NULL, " +
                    "  FOREIGN KEY(ID_NIVEL) REFERENCES NIVELES_IMPORTANCIA_TAREAS)");

        /* Creación de tabla TAREAS_PUNTUALES referente a la entidad TarePuntual.   */
        bd.execSQL("CREATE TABLE TAREAS_PUNTUALES (" +
                    "_id INTEGER NOT NULL, " +
                    " FECHA_HORA NUMERIC NOT NULL, " +
                    " PRIMARY KEY(_id), " +
                    " FOREIGN KEY(_id) REFERENCES TAREAS ON DELETE CASCADE)");

        /* Creación de tabla TAREAS_SEMANALES referente a la entidad TareaSemanal.   */
        bd.execSQL("CREATE TABLE TAREAS_SEMANALES (" +
                    "_id INTEGER NOT NULL, " +
                    "LUNES INTEGER NOT NULL DEFAULT 0, " +
                    "MARTES INTEGER NOT NULL DEFAULT 0, " +
                    "MIERCOLES INTEGER NOT NULL DEFAULT 0, " +
                    "JUEVES INTEGER NOT NULL DEFAULT 0, " +
                    "VIERNES INTEGER NOT NULL DEFAULT 0, " +
                    "SABADO INTEGER NOT NULL DEFAULT 0, " +
                    "DOMINGO INTEGER NOT NULL DEFAULT 0, " +
                    "HORA NUMERIC NOT NULL, " +
                    "FECHA_HORA_SIG_ALARMA NUMERIC NOT NULL DEFAULT 0, " +
                    "PRIMARY KEY(_id), " +
                    "FOREIGN KEY(_id) REFERENCES TAREAS ON DELETE CASCADE)");

        /* Creación de tabla TAREAS_NOTIFICADAS referente a la entidad TareaNotificada.   */
        bd.execSQL("CREATE TABLE TAREAS_NOTIFICADAS (" +
                    "_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "TITULO TEXT NOT NULL, " +
                    "DESCRIPCION TEXT, " +
                    "FECHA_HORA_TAREA NUMERIC NOT NULL, " +
                    "FECHA_HORA_REALIZADO NUMERIC NOT NULL DEFAULT 0, " +
                    "FECHA_HORA_NOTIFICACION NUMERIC NOT NULL DEFAULT 0, " +
                    "ID_NIVEL INTEGER NOT NULL, " +
                    "CANTIDAD_NOTIFICACIONES INTEGER NOT NULL DEFAULT 0, " +
                    "CONFIRMADO INTEGER NOT NULL DEFAULT 0, " +
                    "  FOREIGN KEY(ID_NIVEL) REFERENCES NIVELES_IMPORTANCIA_TAREAS)");

        //TODO Tiempos de prueba
        bd.execSQL("INSERT INTO NIVELES_IMPORTANCIA_TAREAS VALUES (null,'sin notificaciones',0,0,0,'afeeee')");
        bd.execSQL("INSERT INTO NIVELES_IMPORTANCIA_TAREAS VALUES (null,'normal',4,0,3,'ceff91')");
        bd.execSQL("INSERT INTO NIVELES_IMPORTANCIA_TAREAS VALUES (null,'importante',6,0,2,'ffd182');");
        bd.execSQL("INSERT INTO NIVELES_IMPORTANCIA_TAREAS VALUES (null,'critica',7,1,1,'ff8a82')");


    }

    /**
     * Obtención de tareas confirmadas que tienen notificaciones pendientes por realizar.
     * @return Lista tareas notificadas confirmadas.
     */
    public List<TareaNotificada> getTareasConfirmadasPendientes(){
        SQLiteDatabase bd = getReadableDatabase();
        List<TareaNotificada> listaTareas = new ArrayList<>();
        Cursor cursor;
        String consulta = "SELECT TAREAS_NOTIFICADAS._id, TITULO, DESCRIPCION, FECHA_HORA_TAREA, FECHA_HORA_REALIZADO, FECHA_HORA_NOTIFICACION, ID_NIVEL, CANTIDAD_NOTIFICACIONES, CONFIRMADO  " +
                            "FROM TAREAS_NOTIFICADAS INNER JOIN NIVELES_IMPORTANCIA_TAREAS ON TAREAS_NOTIFICADAS.ID_NIVEL=NIVELES_IMPORTANCIA_TAREAS._id  " +
                            "WHERE CONFIRMADO = 1 AND CANTIDAD_NOTIFICACIONES < LIMITE_NOTIFICACIONES ";
        cursor = bd.rawQuery(consulta, null);
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaNotificadaCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Obtención de tareas sin confirmar que tienen notificaciones pendientes por realizar.
     * @return Lista tareas notificadas sin confirmar.
     */
    public List<TareaNotificada> getTareasSinConfirmarPendientes(){
        SQLiteDatabase bd = getReadableDatabase();
        List<TareaNotificada> listaTareas = new ArrayList<>();
        Cursor cursor;
        String consulta = "SELECT TAREAS_NOTIFICADAS._id, TITULO, DESCRIPCION, FECHA_HORA_TAREA, FECHA_HORA_REALIZADO, FECHA_HORA_NOTIFICACION, ID_NIVEL, CANTIDAD_NOTIFICACIONES, CONFIRMADO  " +
                "FROM TAREAS_NOTIFICADAS INNER JOIN NIVELES_IMPORTANCIA_TAREAS ON TAREAS_NOTIFICADAS.ID_NIVEL=NIVELES_IMPORTANCIA_TAREAS._id  " +
                "WHERE CONFIRMADO = 0 AND CANTIDAD_NOTIFICACIONES < LIMITE_NOTIFICACIONES ";
        cursor = bd.rawQuery(consulta, null);
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaNotificadaCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Obtención de todas las tareas confirmadas.
     * @return Lista tareas notificadas.
     */
    public List<TareaNotificada> getAllTareasConfirmadas(){
        SQLiteDatabase bd = getReadableDatabase();
        List<TareaNotificada> listaTareas = new ArrayList<>();
        Cursor cursor;
        String consulta = "SELECT * FROM TAREAS_NOTIFICADAS WHERE CONFIRMADO = 1 ORDER BY FECHA_HORA_TAREA DESC";
        cursor = bd.rawQuery(consulta, null);
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaNotificadaCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Obtención de todas las tareas confirmadas.
     * @return Lista tareas notificadas.
     */
    public List<TareaNotificada> getAllTareasSinConfirmar(){
        SQLiteDatabase bd = getReadableDatabase();
        List<TareaNotificada> listaTareas = new ArrayList<>();
        Cursor cursor;
        String consulta = "SELECT * FROM TAREAS_NOTIFICADAS WHERE CONFIRMADO = 0 ORDER BY FECHA_HORA_TAREA DESC";
        cursor = bd.rawQuery(consulta, null);
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaNotificadaCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Extracción de tarea notificada de cursor pasado.
     * @return tarea notificada estraida del cursor.
     */
    private static TareaNotificada extraeTareaNotificadaCursor(Cursor cursor) {
        TareaNotificada tarea = new TareaNotificada(cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getLong(3),
                cursor.getLong(4),
                cursor.getLong(5),
                cursor.getInt(6),
                cursor.getInt(7),
                cursor.getInt(8) > 0);

        return tarea;
    }

    /**
     * Obtención de todos los niveles de importancia del app.
     * @return Cursor con niveles de importancia.
     */
    public Cursor getAllNiveles(){
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursorRet;
        String consulta = "SELECT * FROM NIVELES_IMPORTANCIA_TAREAS";
        cursorRet = bd.rawQuery(consulta, null);
        return cursorRet;
    }

    /**
     * Insercción de nueva tarea semanal en la base de datos, tanto en tabla TAREA_SEMANAL como en TAREA.
     * @param tarea tarea semanal a introducir en base de datos.
     * @return id de nueva tarea introducida.
     */
    public int nuevaTareaSemanal(TareaSemanal tarea){
        int _id = -1;

        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("INSERT INTO TAREAS (TITULO, DESCRIPCION, ID_NIVEL) VALUES(?,?,?)",
                    new String[] {tarea.getTitulo(),
                tarea.getDescripcion(), String.valueOf(tarea.getNivel_importancia())} );

        Cursor c = bd.rawQuery("SELECT last_insert_rowid()",null);
        if (c.moveToNext()){
            _id = c.getInt(0);
            bd.execSQL("INSERT INTO TAREAS_SEMANALES VALUES(?,?,?,?,?,?,?,?,?,?)",
                        new String[] {String.valueOf(_id),
                                      String.valueOf((tarea.isLunes())? 1 : 0),
                                      String.valueOf((tarea.isMartes())? 1 : 0),
                                      String.valueOf((tarea.isMiercoles())? 1 : 0),
                                      String.valueOf((tarea.isJueves())? 1 : 0),
                                      String.valueOf((tarea.isViernes())? 1 : 0),
                                      String.valueOf((tarea.isSabado())? 1 : 0),
                                      String.valueOf((tarea.isDomingo())? 1 : 0),
                                      String.valueOf((tarea.getHora())),
                                      String.valueOf((tarea.getFechaHoraSigAlarma()))}
                        );
             }
        c.close();
        bd.close();
        return _id;
    }

    /**
     * Insercción de nueva tarea puntual en la base de datos, tanto en tabla TAREA_PUNTUAL como en TAREA.
     * @param tarea tarea puntual a introducir en base de datos.
     * @return id de nueva tarea introducida.
     */
    public int nuevaTareaPuntual(TareaPuntual tarea){
        int _id=-1;

        SQLiteDatabase bd =  getWritableDatabase();
        bd.execSQL("INSERT INTO TAREAS (TITULO, DESCRIPCION, ID_NIVEL) VALUES(?,?,?)",
                    new String[] {tarea.getTitulo(),
                tarea.getDescripcion(), String.valueOf(tarea.getNivel_importancia())} );

        Cursor c = bd.rawQuery("SELECT last_insert_rowid()",null);
        if (c.moveToNext()){
            _id = c.getInt(0);
            bd.execSQL("INSERT INTO TAREAS_PUNTUALES VALUES(?,?)",
                    new String[] {String.valueOf(_id),String.valueOf(tarea.getFechaHora())}
            );
        }
        c.close();
        bd.close();
        return _id;
    }

    /**
     * Insercción de datos de tarea que se ha notificado mediante alarma.
     * @param tareaNotificada tarea que ha sido notificada.
     * @return id de nueva tarea introducida.
     */
    public int nuevaTareaNotificada(TareaNotificada tareaNotificada) {
        int _id =-1;

        SQLiteDatabase bd =  getWritableDatabase();
        bd.execSQL("INSERT INTO TAREAS_NOTIFICADAS (TITULO, DESCRIPCION, FECHA_HORA_TAREA, FECHA_HORA_REALIZADO, FECHA_HORA_NOTIFICACION, " +
                    "ID_NIVEL, CANTIDAD_NOTIFICACIONES, CONFIRMADO) VALUES(?,?,?,?,?,?,?,?)",
                new String[] {tareaNotificada.getTitulo(),
                        tareaNotificada.getDescripcion(),String.valueOf(tareaNotificada.getFechaHoraTarea()),
                        String.valueOf(tareaNotificada.getFechaHoraRealizado()),
                        String.valueOf(tareaNotificada.getFechaHoraNotificacion()),
                        String.valueOf(tareaNotificada.getNivel_importancia()),String.valueOf(tareaNotificada.getCantidad_notificaciones()),
                        String.valueOf((tareaNotificada.isConfirmado())? 1 : 0)}
        );

        Cursor c = bd.rawQuery("SELECT last_insert_rowid()",null);
        if (c.moveToNext()){
            _id = c.getInt(0);
        }
        c.close();
        bd.close();
        return _id;
    }

    /**
     * Obtención de todas las tareas programadas.
     * @return Lista tareas programadas.
     */
    public List<Tarea> getAllTareas(){
        Cursor cursor;
        List<Tarea> listaTareas = new ArrayList<>();

        cursor =  this.getCursorTareasPuntuales();
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaPuntualCursor(cursor));
            } while(cursor.moveToNext());
        }

        cursor =  this.getCursorTareasSemanales();
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaSemanalCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Obtención de todas las tareas semanales.
     * @return Lista tareas semanales en cast Tarea.
     */
    public List<Tarea> getAllTareasSemanales(){
        Cursor cursor;
        List<Tarea> listaTareas = new ArrayList<>();
        cursor =  this.getCursorTareasSemanales();
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaSemanalCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Obtención de todas las tareas puntuales.
     * @return Lista tareas puntuales en cast Tarea.
     */
    public List<Tarea> getAllTareasPuntuales(){
        Cursor cursor;
        List<Tarea> listaTareas = new ArrayList<>();
        cursor =  this.getCursorTareasPuntuales();
        if (cursor.moveToNext()) {
            do {
                listaTareas.add(extraeTareaPuntualCursor(cursor));
            } while(cursor.moveToNext());
        }
        return listaTareas;
    }

    /**
     * Obtención de cursor con las tareas puntuales
     * @return cursor con tareas puntuales.
     */
    private Cursor getCursorTareasPuntuales(){
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursorRet;
        String consulta = "SELECT TAREAS._id, TITULO, DESCRIPCION, FECHA_HORA, ID_NIVEL " +
                          "FROM TAREAS INNER JOIN TAREAS_PUNTUALES ON TAREAS._id=TAREAS_PUNTUALES._id;";
        cursorRet = bd.rawQuery(consulta, null);

        return cursorRet;
    }

    /**
     * Obtención de cursor con las tareas semanales.
     * @return cursor con tareas semanales.
     */
    private Cursor getCursorTareasSemanales(){
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursorRet;
        String consulta = "SELECT TAREAS._id, TITULO, DESCRIPCION, ID_NIVEL, LUNES, MARTES, MIERCOLES, " +
                          "JUEVES, VIERNES, SABADO, DOMINGO, HORA, FECHA_HORA_SIG_ALARMA " +
                          "FROM TAREAS INNER JOIN TAREAS_SEMANALES ON TAREAS._id=TAREAS_SEMANALES._id";
        cursorRet = bd.rawQuery(consulta, null);
        return cursorRet;
    }

    /**
     * Extracción de tarea programada de cursor pasado.
     * @return tarea estraida del cursor.
     */
    private static Tarea extraeTareaCursor(Cursor cursor) {
        return new Tarea(cursor.getInt(0),
                                cursor.getString(1),
                                cursor.getString(2),
                                cursor.getInt(3));

    }

    /**
     * Extracción de nivel de importancia de cursor pasado.
     * @return nivel de importancia estraida del cursor.
     */
    private static NivelImportanciaTarea extraeNivelImportanciaTareaCursor(Cursor cursor) {
        return new NivelImportanciaTarea(
                                    cursor.getInt(0),
                                    cursor.getString(1),
                                    cursor.getInt(2),
                                    cursor.getInt(3) > 0,
                                    cursor.getInt(4),
                                    cursor.getString(5));
    }

    /**
     * Extracción de tarea puntual de cursor pasado.
     * @return tarea puntual estraida del cursor.
     */
    private static Tarea extraeTareaPuntualCursor(Cursor cursor) {
        return new TareaPuntual( cursor.getInt(0),
                                               cursor.getString(1),
                                               cursor.getString(2),
                                               cursor.getLong(3),
                                               cursor.getInt(4)
        );

    }

    /**
     * Extracción de tarea semanal de cursor pasado.
     * @return tarea semanal estraida del cursor.
     */
    private static Tarea extraeTareaSemanalCursor(Cursor cursor) {
        return new TareaSemanal( cursor.getInt(0),
                                               cursor.getString(1),
                                               cursor.getString(2),
                                               cursor.getInt(3),
                                               cursor.getInt(4) > 0,
                                               cursor.getInt(5) > 0,
                                               cursor.getInt(6) > 0,
                                               cursor.getInt(7) > 0,
                                               cursor.getInt(8) > 0,
                                               cursor.getInt(9) > 0,
                                               cursor.getInt(10) > 0,
                                               cursor.getLong(11),
                                               cursor.getLong(12));

    }

    /**
     * Obtención de nivel de importancia solicitado.
     * @param id identificador de nivel de importancia solicitado.
     * @return nivel de importancia.
     */
    public NivelImportanciaTarea getNivelImportanciaTarea(int id){
        NivelImportanciaTarea nivelImportanciaTarea = null;
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT * FROM NIVELES_IMPORTANCIA_TAREAS WHERE _id = " + id, null);
        if (cursor.moveToNext()){
            nivelImportanciaTarea = extraeNivelImportanciaTareaCursor(cursor);
        }
        cursor.close();
        bd.close();
        return nivelImportanciaTarea;
    }

    /**
     * Obtención de tarea configurada solicitada.
     * @param id identificador de tarea programada solicitada.
     * @return tarea programada.
     */
    public Tarea getTareaConfigurada(int id){
        Tarea tarea = null;
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT * FROM TAREAS WHERE _id = " + id,
                null);
        if (cursor.moveToNext()) {
            tarea = extraeTareaCursor(cursor);
        }
        cursor.close();
        bd.close();
        return tarea;
    }

    /**
     * Obtención de tarea notificada solicitada.
     * @param id identificador de tarea notificada solicitada.
     * @return tarea notificada.
     */
    public TareaNotificada getTareaNotificada(int id){
        TareaNotificada tarea = null;
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT * FROM TAREAS_NOTIFICADAS WHERE _id = " + id,
                null);
        if (cursor.moveToNext()) {
            tarea = extraeTareaNotificadaCursor(cursor);
        }
        cursor.close();
        bd.close();
        return tarea;
    }

    /**
     * Obtención de tarea puntual solicitada.
     * @param id identificador de tarea puntual solicitada.
     * @return tarea puntual.
     */
    public TareaPuntual getTareaPuntual(int id){
        TareaPuntual tarea = null;
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT TAREAS._id, TITULO, DESCRIPCION, FECHA_HORA, ID_NIVEL " +
                                    "FROM TAREAS INNER JOIN TAREAS_PUNTUALES ON TAREAS._id=TAREAS_PUNTUALES._id " +
                                    "WHERE TAREAS._id = " + id,
                null);
        if (cursor.moveToNext()) {
            tarea = (TareaPuntual) extraeTareaPuntualCursor(cursor);
        }
        cursor.close();
        bd.close();
        return tarea;
    }

    /**
     * Obtención de tarea semanal solicitada.
     * @param id identificador de tarea semanal solicitada.
     * @return tarea semanal.
     */
    public TareaSemanal getTareaSemanal(int id){
        TareaSemanal tarea = null;
        SQLiteDatabase bd = getReadableDatabase();
        Cursor cursor = bd.rawQuery("SELECT TAREAS._id, TITULO, DESCRIPCION, ID_NIVEL, LUNES, " +
                        "MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO, HORA, FECHA_HORA_SIG_ALARMA " +
                        "FROM TAREAS INNER JOIN TAREAS_SEMANALES ON TAREAS._id=TAREAS_SEMANALES._id " +
                        "WHERE TAREAS._id = " + id,
                null);
        if (cursor.moveToNext()) {
            tarea = (TareaSemanal) extraeTareaSemanalCursor(cursor);
        }
        cursor.close();
        bd.close();
        return tarea;
    }

    /**
     * Modificación datos de tarea puntual ya en base de datos.
     * @param tarea con datos modificados a introducir en base de datos.
     */
    public void actualizaTareaPuntual(TareaPuntual tarea) {
        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("UPDATE TAREAS SET TITULO = '" + tarea.getTitulo() +
                "', DESCRIPCION = '" + tarea.getDescripcion() +
                "', ID_NIVEL = " + tarea.getNivel_importancia() +
                " WHERE _id = " + tarea.getId_tarea());

        bd.execSQL("UPDATE TAREAS_PUNTUALES SET FECHA_HORA = " + tarea.getFechaHora() +
                " WHERE _id = " + tarea.getId_tarea());
        bd.close();
    }

    /**
     * Modificación datos de tarea semanal ya en base de datos.
     * @param tarea con datos modificados a introducir en base de datos.
     */
    public void actualizaTareaSemanal(TareaSemanal tarea) {
        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("UPDATE TAREAS SET TITULO = '" + tarea.getTitulo() +
                "', DESCRIPCION = '" + tarea.getDescripcion() +
                "', ID_NIVEL = " + tarea.getNivel_importancia() +
                " WHERE _id = " + tarea.getId_tarea());

        bd.execSQL("UPDATE TAREAS_SEMANALES SET LUNES = " + String.valueOf((tarea.isLunes())? 1 : 0) +
                ", MARTES = " + String.valueOf((tarea.isMartes())? 1 : 0) +
                ", MIERCOLES = " + String.valueOf((tarea.isMiercoles())? 1 : 0) +
                ", JUEVES = " + String.valueOf((tarea.isJueves())? 1 : 0) +
                ", VIERNES = " + String.valueOf((tarea.isViernes())? 1 : 0) +
                ", SABADO = " + String.valueOf((tarea.isSabado())? 1 : 0) +
                ", DOMINGO = " + String.valueOf((tarea.isDomingo())? 1 : 0) +
                ", HORA = " + tarea.getHora() +
                ", FECHA_HORA_SIG_ALARMA = " + tarea.getFechaHoraSigAlarma() +
                " WHERE _id = " + tarea.getId_tarea());
        bd.close();
    }

    /**
     * Incrementación de cantidad de notificaciones realizadas sobre una tarea notificad y actualización
     * de fecha y hora de siguiente notificación
     * @param id identificación de tarea notificada.
     * @param fecha_hora_notificacion instante en milisegundos que indica fecha y hora de siguiente notificación.
     */
    public void incrementarCantidadNotificaciones(int id, long fecha_hora_notificacion){
        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("UPDATE TAREAS_NOTIFICADAS SET CANTIDAD_NOTIFICACIONES=CANTIDAD_NOTIFICACIONES+1, FECHA_HORA_NOTIFICACION = " +  String.valueOf(fecha_hora_notificacion) +
                    " WHERE _id = " + id);
        bd.close();
    }

    /**
     * Confirmación de tarea notificada.
     * @param id identificación de tarea notificada.
     * @param fecha_hora_realizado instante en milisegundos que indica fecha y hora de realización de tarea.
     * @param fecha_hora_notificacion instante en milisegundos que indica fecha y hora de siguiente notificación.
     */
    public void confirmarTarea(int id, long fecha_hora_realizado, long fecha_hora_notificacion ){
        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("UPDATE TAREAS_NOTIFICADAS SET CANTIDAD_NOTIFICACIONES=0, CONFIRMADO=1, FECHA_HORA_REALIZADO = " + String.valueOf(fecha_hora_realizado) +
               ", " + "FECHA_HORA_NOTIFICACION = " + String.valueOf(fecha_hora_notificacion) + " WHERE _id = " + id);
        bd.close();
    }

    public int tamanyo() {
        return 0;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                                    int newVersion) {
    }

    /**
     * Borrado de tarea programada.
     * @param _id identificación de tarea notificada.
     */
    public void borrarTarea(int _id) {
        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("DELETE FROM TAREAS WHERE _id = " + _id);
        bd.close();
    }

    /**
     * Borrado de todas las tareas notificadas confirmadas.
     */
    public void borrarTareasConfirmadas(){
        SQLiteDatabase bd = getWritableDatabase();
        bd.execSQL("DELETE FROM TAREAS_NOTIFICADAS WHERE CONFIRMADO=1;");
        bd.close();

    }


}
