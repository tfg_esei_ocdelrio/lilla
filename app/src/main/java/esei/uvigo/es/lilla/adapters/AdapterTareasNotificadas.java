package esei.uvigo.es.lilla.adapters;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.MostrarTareaNotificada;
import esei.uvigo.es.lilla.R;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;

/**
 * Created by Oscar Campos del Río on 07/02/2017.
 */

/**
 * Clase extendida de ReciclerView para el mostrado de las tares notificadas
 */
public class AdapterTareasNotificadas extends RecyclerView.Adapter<AdapterTareasNotificadas.ViewHolder> {
    protected Context contexto;
    protected LayoutInflater inflador;
    protected View.OnClickListener onClickListener;

    public AdapterTareasNotificadas(Context contexto) {
        this.contexto = contexto;
        inflador = (LayoutInflater) contexto
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    //Creamos nuestro ViewHolder, con los tipos de elementos a modificar
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_card_titulo_tarea, tv_card_hora_tarea,
                tv_card_descripcion_tarea, tv_card_fecha_tarea, tv_card_id_tarea;
        public CardView card_tarea;
        public ImageView iv_card_importancia;

        public ViewHolder(View itemView, Context contexto) {
            super(itemView);
            final Context contextoV = contexto;
            tv_card_titulo_tarea = (TextView) itemView.findViewById(R.id.tv_card_titulo_tarea);
            tv_card_hora_tarea = (TextView) itemView.findViewById(R.id.tv_card_hora_tarea);
            tv_card_descripcion_tarea = (TextView) itemView.findViewById(R.id.tv_card_descripcion_tarea);
            tv_card_fecha_tarea = (TextView) itemView.findViewById(R.id.tv_card_fecha_tarea);
            tv_card_id_tarea = (TextView) itemView.findViewById(R.id.tv_card_id_tarea);
            card_tarea = (CardView) itemView.findViewById(R.id.card_tarea);
            iv_card_importancia = (ImageView) itemView.findViewById(R.id.iv_card_importancia);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Intent i;
                            i = new Intent(contextoV, MostrarTareaNotificada.class);
                            i.putExtra("id", Integer.valueOf(tv_card_id_tarea.getText().toString()));
                            contextoV.startActivity(i);
                }
            });
        }

}
    /**
     * Creamos el ViewHolder con la vista de un elemento sin personalizar
     * @param parent
     * @param viewType
     * @return Viewholder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflamos la vista desde el xml
        View v = inflador.inflate(R.layout.elemento_tarea_notificada_lista, parent, false);
        v.setOnClickListener(onClickListener);
        return new ViewHolder(v,contexto);
    }

    /**
     * Usando como base el ViewHolder y lo personalizamos
     * @param holder
     * @param posicion
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        TareaNotificada tarea = ((Lilla) contexto.getApplicationContext()).tareasBD.getTareaNotificada(posicion);
        NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) contexto.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
        personalizaVista(holder, tarea, nivelImportanciaTarea);
    }

    /**
     * Personalización del ViewHolder
     * @param holder
     * @param tarea
     */
    public void personalizaVista(ViewHolder holder, TareaNotificada tarea, NivelImportanciaTarea nivelImportanciaTarea) {
        holder.tv_card_id_tarea.setText(String.valueOf(tarea.getId_tarea_notificada()));
        holder.tv_card_titulo_tarea.setText(tarea.getTitulo());
        Calendar fecha_hora =  Calendar.getInstance(TimeZone.getDefault());
        fecha_hora.setTimeInMillis(tarea.getFechaHoraTarea());
        holder.tv_card_hora_tarea.setText(String.format("%02d", fecha_hora.get(Calendar.HOUR_OF_DAY)) +
                ":" + String.format("%02d", fecha_hora.get(Calendar.MINUTE)));
        holder.tv_card_descripcion_tarea.setText(tarea.getDescripcion());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        holder.tv_card_fecha_tarea.setText(dateFormatter.format(fecha_hora.getTime()));
        holder.card_tarea.setCardBackgroundColor(Color.parseColor("#" + nivelImportanciaTarea.getColor()));

        /* Si el nivel importancia indica que se avisaría al cuidador principal, mostramos icono de
         * importancia. */
        if(nivelImportanciaTarea.isAviso_cp())
            holder.iv_card_importancia.setVisibility(View.VISIBLE);
        else
            holder.iv_card_importancia.setVisibility(View.INVISIBLE);
        }



    // Indicamos el número de elementos de la lista
    @Override public int getItemCount() {
        return ((Lilla) contexto.getApplicationContext()).tareasBD.tamanyo();
    }
}
