package esei.uvigo.es.lilla.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;

import esei.uvigo.es.lilla.GmailSend;
import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.MostrarTareaNotificada;
import esei.uvigo.es.lilla.R;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;

import esei.uvigo.es.lilla.entities.TareaNotificada;

/**
 * Created by Oscar Campos del Río on 16/02/2017.
 */

/**
 * Receiver alarma establecida en AlarmManager para mostrar notificaciones de tarea notificada
 * confirmada.
 */
public class ReceiverNotificationTareaSinConfirmar extends WakefulBroadcastReceiver {
    private Context context;
    private TareaNotificada tareaNotificada;


    @Override
    public void onReceive(Context c, Intent i) {
        this.context=c;
        NotificationManager mNM;
        mNM = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        Uri soundUri = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Bundle params = i.getExtras();

        /* Obtenemos tarea notificada confirmada. */
        tareaNotificada = ((Lilla) context.getApplicationContext()).tareasBD.getTareaNotificada(params.getInt("id"));


        /* Si quedan notificaciones pentiendes de realizar se ejecuta código.  */
        if(tareaNotificada.getCantidad_notificaciones() < params.getInt("limiteNotificaciones")){
            Calendar fechaHoraNotificacion = Calendar.getInstance(TimeZone.getDefault());
            fechaHoraNotificacion.add(Calendar.MINUTE,params.getInt("minutos"));
            tareaNotificada.setFechaHoraNotificacion(fechaHoraNotificacion.getTimeInMillis());
            tareaNotificada.setCantidad_notificaciones(tareaNotificada.getCantidad_notificaciones()+1);
            ((Lilla) context.getApplicationContext()).tareasBD.incrementarCantidadNotificaciones(tareaNotificada.getId_tarea_notificada(),fechaHoraNotificacion.getTimeInMillis());

        /* Si se notifico el limite de veces se comprueba el enviar correo a cuidador personal, sino comfigura siguiente notificacion **/
        if(tareaNotificada.getCantidad_notificaciones() == params.getInt("limiteNotificaciones")) {
            if (params.getBoolean("avisoCP")) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                if (prefs.getBoolean("notificacion_cp_email", false)) {
                    Intent intentEmail = new Intent(context, GmailSend.class);
                    intentEmail.putExtra("titulo", context.getResources().getString(R.string.titulo_aviso_email_cp) + " " + tareaNotificada.getTitulo());
                    intentEmail.putExtra("cuerpo", componerCuerpoEmail());
                    intentEmail.putExtra("correo_tarea", true);
                    intentEmail.putExtra("id_tarea", tareaNotificada.getId_tarea_notificada());
                    intentEmail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK + Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(intentEmail);
                }
            }
        }else {
            FunctionsAlarmManager.establecerNotificacionTareaSinConfirmar(tareaNotificada,
                    params.getInt("minutos"),
                    params.getInt("limiteNotificaciones"),
                    params.getBoolean("avisoCP"),
                    context);
        }

        /* Creamos notificación y la mostramos */
        Calendar hora = Calendar.getInstance(java.util.TimeZone.getDefault());
        hora.setTimeInMillis(tareaNotificada.getFechaHoraTarea());
        Intent intent = new Intent(context, MostrarTareaNotificada.class);
        intent.putExtra("id",tareaNotificada.getId_tarea_notificada());
        PendingIntent pIntent = PendingIntent.getActivity(context, tareaNotificada.getId_tarea_notificada() , intent, PendingIntent.FLAG_CANCEL_CURRENT);
        String mensaje = params.getString("titulo") + "\r\n" + context.getResources().getString(R.string.text_hora_notificacion) +
                " " + String.format("%02d", hora.get(Calendar.HOUR_OF_DAY)) +
                ":" + String.format("%02d", hora.get(Calendar.MINUTE));
        Notification notification = new Notification.Builder(context)
                .setContentTitle(context.getResources().getString(R.string.confirmar_tarea))
                .setContentText(mensaje)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setVibrate(new long[] {200, 1000, 250, 1000, 2000,1000, 250, 1000, 2000, })
                .setLights(Color.RED,1000,1000)
                .setStyle(new Notification.BigTextStyle()
                        .bigText(mensaje))
                .build();
        mNM.notify(tareaNotificada.getId_tarea_notificada(),notification);
        }
    }

    /* Composicion de cuerpo de email para cuidador principal. */
    private String componerCuerpoEmail(){
        String ret;

        Calendar fecha_hora =  Calendar.getInstance(TimeZone.getDefault());
        fecha_hora.setTimeInMillis(tareaNotificada.getFechaHoraTarea());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
        ret=    context.getResources().getString(R.string.titulo_cuerpo_email_cp) + "\n" +
                tareaNotificada.getTitulo() + "\n" +
                tareaNotificada.getDescripcion()+ "\n" +
                (String.format("%02d", fecha_hora.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", fecha_hora.get(Calendar.MINUTE))) + " " +
                (dateFormatter.format(fecha_hora.getTime())) + " " +  TimeZone.getDefault().getDisplayName();

        return ret;
    }


}



