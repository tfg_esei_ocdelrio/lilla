package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 09/11/2016.
 */

import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.v7.app.AlertDialog;

import java.util.List;
import java.util.ListIterator;

import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;




/**
 * Fragment perteneciente PreferenciasActivity
 */
public class PreferenciasFragment extends PreferenceFragment  {
    public CheckBoxPreference notificacionCpEmail;
    public EditTextPreference correoNotificacionCp;
    public PreferenceScreen  borrarHistorial;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);


        notificacionCpEmail = (CheckBoxPreference) findPreference("notificacion_cp_email");
        correoNotificacionCp = (EditTextPreference) findPreference("correo_notificacion_cp");
        borrarHistorial = (PreferenceScreen) findPreference("borrar_historial");


        /* Mostar valor del EditTextPreference en el summary .*/
        correoNotificacionCp.setSummary(correoNotificacionCp.getText());
        correoNotificacionCp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener(){
            public boolean onPreferenceChange(Preference preference,
                                              Object newValue){
                correoNotificacionCp.setSummary((String) newValue  );
                return true;
            }
        });

        /* Si esta pulsado notificacionCpEmail no se permite cambiar el correo del cuidador principal  */
        if(notificacionCpEmail.isChecked()) {
            correoNotificacionCp.setSelectable(false);
        }else{
            correoNotificacionCp.setSelectable(true);
        }

        /* Si se pulsa notificacionCpEmail y esta a true se crea intent para pedir permisos a la API GMAIL y certificar el cuidador principal.  */
        notificacionCpEmail.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                notificacionCpEmail.setSelectable(false);
                if(notificacionCpEmail.isChecked()) {
                    Intent intentEmail  = new Intent(getActivity().getApplicationContext(), GmailSend.class);
                    intentEmail.putExtra("titulo",getResources().getString(R.string.titulo_confirmacion_email_cp) );
                    intentEmail.putExtra("cuerpo", correoNotificacionCp.getText() + " " + getResources().getString(R.string.cuerpo_confirmacion_email_cp));

                    PreferenciasActivity pA = (PreferenciasActivity) getActivity();
                    pA.envioCorreoEleccionCP(intentEmail);

                }else{
                    correoNotificacionCp.setSelectable(true);
                }
                notificacionCpEmail.setSelectable(true);
                return true;
            }
        });

        /* Si se pulsa borrarHistorial se pedira confirmar para eliminar todas las tareas ya confirmadas */
        borrarHistorial.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getResources().getString(R.string.dialog_borrar_historial))
                        .setMessage(getResources().getString(R.string.mensaje_borrar_historial))
                        .setPositiveButton(getResources().getString(R.string.confirmar_alert_dialog), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                        borrarHistorial();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancelar_alert_dialog), null)
                        .show();
                return true;
            }
        });
    }

    private void borrarHistorial(){
        List<TareaNotificada> listaTareasConfirmadas;
        NotificationManager notifManager= (NotificationManager) getActivity().getApplicationContext().getSystemService(getActivity().getApplicationContext().NOTIFICATION_SERVICE);
        listaTareasConfirmadas = ((Lilla)  getActivity().getApplication()).tareasBD.getAllTareasConfirmadas();
        ListIterator<TareaNotificada> itTareasConfirmada = listaTareasConfirmadas.listIterator();
        while(itTareasConfirmada.hasNext()) {
            TareaNotificada tarea = itTareasConfirmada.next();
            notifManager.cancel(tarea.getId_tarea_notificada());
            NivelImportanciaTarea nivelImportanciaTarea = ((Lilla)  getActivity().getApplication()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
            FunctionsAlarmManager.borrarNotificacionTareaConfirmada(tarea,nivelImportanciaTarea.getMinutos(),nivelImportanciaTarea.getLimite_notificaciones(),getActivity().getApplicationContext());
        }
                                /* Borramos tareas de base de datos. */
        ((Lilla)  getActivity().getApplication()).tareasBD.borrarTareasConfirmadas();

                        /* Actualizamos ReciclerViews de tareas confirmadas  en caso de que esté iniciada. */
        if(MainActivity.PlaceholderFragment.tareasConfirmadasAdapter!= null ){
            MainActivity.PlaceholderFragment.tareasConfirmadasAdapter.setList( ((Lilla)  getActivity().getApplication()).tareasBD.getAllTareasConfirmadas());
            MainActivity.PlaceholderFragment.tareasConfirmadasAdapter.notifyDataSetChanged();
        }
    }
}





