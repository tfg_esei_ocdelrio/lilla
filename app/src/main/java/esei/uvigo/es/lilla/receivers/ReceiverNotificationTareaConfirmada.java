package esei.uvigo.es.lilla.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.util.Calendar;
import java.util.TimeZone;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.MostrarTareaNotificada;
import esei.uvigo.es.lilla.R;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.entities.TareaNotificada;

/**
 * Created by Oscar Campos del Río on 21/02/2017.
 */

/**
 * Receiver alarma establecida en AlarmManager para mostrar notificaciones de tarea notificada
 * confirmada.
 */
public class ReceiverNotificationTareaConfirmada extends WakefulBroadcastReceiver {
    private TareaNotificada tareaNotificada;
    @Override
    public void onReceive(Context context, Intent i) {

        NotificationManager mNM;
        mNM = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        Uri soundUri = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bundle params = i.getExtras();

        /* Obtenemos tarea notificada confirmada. */
        tareaNotificada = ((Lilla) context.getApplicationContext()).tareasBD.getTareaNotificada(params.getInt("id"));


        /* Si quedan notificaciones pentiendes de realizar se ejecuta código.  */
        if(tareaNotificada.getCantidad_notificaciones() < params.getInt("limiteNotificaciones")){
            Calendar fechaHoraNotificacion = Calendar.getInstance(TimeZone.getDefault());
            fechaHoraNotificacion.add(Calendar.MINUTE,params.getInt("minutos"));
            tareaNotificada.setFechaHoraNotificacion(fechaHoraNotificacion.getTimeInMillis());

            /* Establecememos nueva alarm en AlarmManager para generar notificación. */
            FunctionsAlarmManager.establecerNotificacionTareaConfirmada(tareaNotificada,
                    params.getInt("minutos"),
                    params.getInt("limiteNotificaciones"),
                    context);

            /* Incrementamos la cantidad de notificaciones realizadas. */
            ((Lilla) context.getApplicationContext()).tareasBD.incrementarCantidadNotificaciones(params.getInt("id"), fechaHoraNotificacion.getTimeInMillis());


            /* Creamos notificación y la mostramos */
            Calendar hora = Calendar.getInstance(java.util.TimeZone.getDefault());
            hora.setTimeInMillis(tareaNotificada.getFechaHoraTarea());
            Intent intent = new Intent(context, MostrarTareaNotificada.class);
            intent.putExtra("id",tareaNotificada.getId_tarea_notificada());
            PendingIntent pIntent = PendingIntent.getActivity(context, tareaNotificada.getId_tarea_notificada() , intent, PendingIntent.FLAG_CANCEL_CURRENT);
            String mensaje = params.getString("titulo") + "\r\n" +
                    context.getResources().getString(R.string.text_hora_notificacion) +
                    " " + String.format("%02d", hora.get(Calendar.HOUR_OF_DAY)) +
                    ":" + String.format("%02d", hora.get(Calendar.MINUTE));
            Notification notification = new Notification.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.tarea_confirmada))
                    .setContentText( mensaje )
                    .setSmallIcon(android.R.drawable.ic_dialog_info)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setVibrate(new long[] {200, 300, 100, 300, 100, 300, 1000, 300, 100, 300, 100, 300, 100})
                    .setLights(Color.GREEN,3000,10000)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(mensaje))
                    .build();
            mNM.notify(tareaNotificada.getId_tarea_notificada(),notification);
        }
    }
}