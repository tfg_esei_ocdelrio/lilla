package esei.uvigo.es.lilla.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.ListIterator;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.Tarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;
import esei.uvigo.es.lilla.entities.TareaPuntual;
import esei.uvigo.es.lilla.entities.TareaSemanal;

/**
 * Created by Oscar Campos del Río on 25/03/2017.
 */

/**
 * Clase servicio para regenerar todas las alarmas de tareas y notificaciones en caso de que el
 * sistema haya liberado memoria. Se relanzará automáticamente al ser finalizado el servicio.
 */
public class LillaAlarmsService extends Service {


    public void onCreate(){
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        //TODO eliminar
        Toast.makeText(getApplicationContext(), "LillaAlarmsService onStart.", Toast.LENGTH_LONG).show();
        Log.d("D", "LillaAlarmsService onStart.");


        List<Tarea> listaTareasSemanales;
        List<Tarea> listaTareasPuntuales;



        /* Obtenemos alarmas semanales. */
        listaTareasSemanales = ((Lilla) getApplication()).tareasBD.getAllTareasSemanales();

        /* Recorremos las tareas semanales para crear alarmas. */
        ListIterator<Tarea> itTareasSemanales = listaTareasSemanales.listIterator();
        while(itTareasSemanales.hasNext()) {
            TareaSemanal tarea = (TareaSemanal) itTareasSemanales.next();
            FunctionsAlarmManager.establecerAlarmaInternaSemanal(tarea, getApplicationContext());
        }

        /* Obtenemos alarmas puntuales. */
        listaTareasPuntuales = ((Lilla) getApplication()).tareasBD.getAllTareasPuntuales();

        /* Recorremos las tareas puntuales para crear alarmas. */
        ListIterator<Tarea> itTareasPuntuales = listaTareasPuntuales.listIterator();
        while(itTareasPuntuales.hasNext()) {
            TareaPuntual tarea = (TareaPuntual) itTareasPuntuales.next();
            FunctionsAlarmManager.establecerAlarmaInternaPuntual(tarea,getApplicationContext());
        }


        /* Recorremos las tareas notificadas confirmadas para crear crear alarmas de notificaciones. */
        List<TareaNotificada> listaTareasConfirmadas;
        listaTareasConfirmadas = ((Lilla) getApplication()).tareasBD.getTareasConfirmadasPendientes();


        /* Recorremos las tareas notificadas confirmadas para crear notificaciones */
        ListIterator<TareaNotificada> itTareasConfirmada = listaTareasConfirmadas.listIterator();
        while(itTareasConfirmada.hasNext()) {
            TareaNotificada tarea = itTareasConfirmada.next();
            NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) getApplication()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
            FunctionsAlarmManager.establecerNotificacionTareaConfirmada(tarea,nivelImportanciaTarea.getMinutos(),nivelImportanciaTarea.getLimite_notificaciones(),getApplicationContext());
        }

        /* Recorremos las tareas notificadas sin confirmar para crear crear alarmas de notificaciones. */
        List<TareaNotificada> listaTareasSinConfirmar;
        listaTareasSinConfirmar =  ((Lilla) getApplication()).tareasBD.getTareasSinConfirmarPendientes();

        /* Recorremos las tareas notificadas sin confirmar para crear notificaciones. */
        ListIterator<TareaNotificada> itTareasSinConfirmar = listaTareasSinConfirmar.listIterator();
        while(itTareasSinConfirmar.hasNext()) {
            TareaNotificada tarea = itTareasSinConfirmar.next();
            NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) getApplication()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
            FunctionsAlarmManager.establecerNotificacionTareaSinConfirmar(tarea,nivelImportanciaTarea.getMinutos(),nivelImportanciaTarea.getLimite_notificaciones(), nivelImportanciaTarea.isAviso_cp(),getApplicationContext());
        }


        /*Se relanzará automáticamente al ser finalizado el servicio al return START_STICKY */
        return START_STICKY;
    }

    public void onDestroy(){
        super.onDestroy();
        //TODO eliminar
        Toast.makeText(getApplicationContext(), "LillaAlarmsService onDestroy.", Toast.LENGTH_LONG).show();
        Log.d("D", "LillaAlarmsService onDestroy.");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }


}