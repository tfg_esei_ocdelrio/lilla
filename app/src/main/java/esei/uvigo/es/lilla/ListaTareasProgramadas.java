package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 13/03/2017.
 */


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import esei.uvigo.es.lilla.adapters.AdapterTareasBD;
import esei.uvigo.es.lilla.database.TareasBD;

/**
 * Activity para mostrado de tareas programadas.
 */
public class ListaTareasProgramadas extends AppCompatActivity {
    public static AdapterTareasBD tareasAdapter;
    private RecyclerView tareasRecyclerView;
    private RecyclerView.LayoutManager tareasLayoutManager;
    private FloatingActionMenu fActionMenuAnyadirTarea;
    private FloatingActionButton fActionButtonAnyadirTareaPuntual, fActionButtonAnyadirTareaSemanal;
    private TextView tvSinElementos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tareas_programadas);

        /*
         * Declaración de RecyclerView, LayoutManager y AdaptersTareasBD donde se cargarán
         * los datos de las tareas programadas.
         */
        tareasRecyclerView = (RecyclerView) findViewById(R.id.recicler_view_tareas_configuradas);
        tareasLayoutManager = new LinearLayoutManager(this);
        tareasAdapter = new AdapterTareasBD(this,  ((Lilla) getApplication()).tareasBD.getAllTareas());
        tareasRecyclerView.setAdapter(tareasAdapter);
        tareasRecyclerView.setLayoutManager(tareasLayoutManager);

        /* FloatingActionButton desplegable para añadido de nuevas tareas. */
        fActionMenuAnyadirTarea = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu_anyadir_tarea);
        fActionButtonAnyadirTareaPuntual = (FloatingActionButton) findViewById(R.id.material_design_floating_action_anyadir_tarea_puntual);
        fActionButtonAnyadirTareaSemanal = (FloatingActionButton) findViewById(R.id.material_design_floating_action_anyadir_tarea_semanal);
        tvSinElementos = (TextView)  findViewById(R.id.tv_sin_elementos);

        /*  Listener FloatingActionButtona añadido de tarea puntual. */
        fActionButtonAnyadirTareaPuntual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(ListaTareasProgramadas.this, EdicionTareaPuntual.class);
                i.putExtra("_id", -1);
                startActivity(i);
            }
        });

        /*  Listener FloatingActionButtona añadido de tarea semanal. */
        fActionButtonAnyadirTareaSemanal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(ListaTareasProgramadas.this, EdicionTareaSemanal.class);
                i.putExtra("_id", -1);
                startActivity(i);
            }
        });

        /* Si no hay elementos mostramos TextView indicandolo. */
        if(tareasAdapter.getItemCount()==0)
            tvSinElementos.setVisibility(View.VISIBLE);
        else
            tvSinElementos.setVisibility(View.GONE);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lista_tareas_programadas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            lanzarPreferencias(null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void lanzarPreferencias(View view) {
        Intent i = new Intent(this, PreferenciasActivity.class);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
        /* Si no hay elementos mostramos TextView indicandolo. */
        if(tareasAdapter.getItemCount()==0)
            tvSinElementos.setVisibility(View.VISIBLE);
        else
            tvSinElementos.setVisibility(View.GONE);
    }

}
