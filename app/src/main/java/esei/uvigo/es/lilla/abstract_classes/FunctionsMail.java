package esei.uvigo.es.lilla.abstract_classes;

/**
 * Created by Google, based in example of @see <a href="https://developers.google.com/gmail/api/guides/sending">Sending Email</a>
 */

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public abstract class FunctionsMail {

    /**
     * Crea clase MimeMessage utilizando los parámetros proporcionados.
     *
     * @param to direccion email del receptor.
     * @param from direccion email del remitente, la cuenta del buzón.
     * @param subject titulo del email
     * @param bodyText cuerpo del email.
     * @return el MimeMessage a ser usado en el envío del correo.
     * @throws MessagingException
     */
    public static MimeMessage createEmail(String to,
                                          String from,
                                          String subject,
                                          String bodyText)
            throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        email.setSubject(subject);
        email.setText(bodyText);
        return email;
    }



    /**
     * Crea clase Message de un email.
     *
     * @param emailContent Email para que se establezca en crudo un Message.
     * @return Un mensaje que contiene un email codificado en base64url.
     * @throws IOException
     * @throws MessagingException
     */
    public static Message createMessageWithEmail(MimeMessage emailContent)
            throws MessagingException, IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }


    /**
     * Envio un email desde el buzon del usuario a su destinario.
     *
     * @param service Instancia de API Gmail autorizada.
     * @param userId Dirección email del uisuario. El valor especial "me" se puede utilizar para
     *               indicar la autentificación del usuario.
     * @param emailContent Email a enviar.
     * @return El mensaje enviado.
     * @throws MessagingException
     * @throws IOException
     */
    public static Message sendMessage(Gmail service,
                                      String userId,
                                      MimeMessage emailContent)
            throws MessagingException, IOException {
        Message message = createMessageWithEmail(emailContent);
        message = service.users().messages().send(userId, message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());
        return message;
    }







}
