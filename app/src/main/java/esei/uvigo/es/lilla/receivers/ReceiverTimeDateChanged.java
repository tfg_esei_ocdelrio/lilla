package esei.uvigo.es.lilla.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.abstract_classes.FunctionsCalendar;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.Tarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;
import esei.uvigo.es.lilla.entities.TareaSemanal;

/**
 * Created by Oscar Campos del Río on 16/02/2017.
 */

/**
 * Receiver para recálculo de tareas cuando se cambie la fecha u hora en el terminal.
 */
public class ReceiverTimeDateChanged extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                action.equals(Intent.ACTION_DATE_CHANGED))
        {
            /* Obtenemos todas las tareas semanalesy  tareas notificadas. */
            List<Tarea> listaTareasSemanales;
            List<TareaNotificada> listaTareasConfirmadas;
            List<TareaNotificada> listaTareasSinConfirmar;

            listaTareasSemanales = ((Lilla) context.getApplicationContext()).tareasBD.getAllTareasSemanales();
            listaTareasConfirmadas = ((Lilla) context.getApplicationContext()).tareasBD.getTareasConfirmadasPendientes();
            listaTareasSinConfirmar = ((Lilla) context.getApplicationContext()). tareasBD.getTareasSinConfirmarPendientes();

            /* Recorremos las tareas semanales para recalcular las alarmas. **/
            ListIterator<Tarea> itTareasSemanales = listaTareasSemanales.listIterator();
            while(itTareasSemanales.hasNext()) {
                TareaSemanal tarea = (TareaSemanal) itTareasSemanales.next();

                Calendar diaAlarma = FunctionsCalendar.calculoFechaSemanaAlarma(tarea);
                Calendar hora = Calendar.getInstance(TimeZone.getDefault());
                hora.setTimeInMillis(tarea.getHora());
                diaAlarma.set(Calendar.HOUR_OF_DAY,hora.get(Calendar.HOUR_OF_DAY));
                diaAlarma.set(Calendar.MINUTE,hora.get(Calendar.MINUTE));
                diaAlarma.set(Calendar.SECOND,hora.get(Calendar.SECOND));
                diaAlarma.set(Calendar.MILLISECOND,hora.get(Calendar.MILLISECOND));
                tarea.setFechaHoraSigAlarma(diaAlarma.getTimeInMillis());
                ((Lilla) context.getApplicationContext()).tareasBD.actualizaTareaSemanal(tarea);
                FunctionsAlarmManager.establecerAlarmaInternaSemanal(tarea, context);


            }
            /* Recorremos las tareas confirmadas y creamos notificaciones- */
            ListIterator<TareaNotificada> itTareasConfirmada = listaTareasConfirmadas.listIterator();
                while(itTareasConfirmada.hasNext()) {
                    TareaNotificada tarea = itTareasConfirmada.next();
                    NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) context.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
                    FunctionsAlarmManager.establecerNotificacionTareaConfirmada(tarea,nivelImportanciaTarea.getMinutos(),nivelImportanciaTarea.getLimite_notificaciones(),context);
                }
            /* Recorremos las tareas sin confirmar y creamos notificaciones. */
            ListIterator<TareaNotificada> itTareasSinConfirmar = listaTareasSinConfirmar.listIterator();
            while(itTareasSinConfirmar.hasNext()) {
                TareaNotificada tarea = itTareasSinConfirmar.next();
                NivelImportanciaTarea nivelImportanciaTarea = ((Lilla) context.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
                FunctionsAlarmManager.establecerNotificacionTareaSinConfirmar(tarea,nivelImportanciaTarea.getMinutos(),nivelImportanciaTarea.getLimite_notificaciones(),nivelImportanciaTarea.isAviso_cp(),context);
            }
        }
    }
}
