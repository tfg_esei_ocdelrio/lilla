package esei.uvigo.es.lilla;

/**
 * Created by Oscar Campos del Río on 13/03/2017.
 */


import android.app.Application;
import android.content.Context;

import esei.uvigo.es.lilla.database.TareasBD;

/**
 * Clase extendida de Application para acceso a base de datos.
 */
public class Lilla extends Application {
    public static Lilla instance = null;
    public TareasBD tareasBD;

    public static Context getInstance() {
        if (null == instance) {
            instance = new Lilla();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        tareasBD = new TareasBD(this);
    }

}
