package esei.uvigo.es.lilla.abstract_classes;

import java.util.Calendar;
import java.util.TimeZone;
import esei.uvigo.es.lilla.entities.TareaSemanal;

/**
 * Created by Oscar Campos del Río on 15/02/2017.
 */

/**
 * Clase abstracta con funciones sobre calculos y operaciones sobre fechas.
 */
public abstract class FunctionsCalendar {

    /**
     * Calculo de dia la semana en la que debe establecerse una alarma.
     * @param tarea tarea semanal sobre la que calcular el día.
     * @return calendar con fecha para establecer alarma.
     */
    static public Calendar calculoFechaSemanaAlarma(TareaSemanal tarea){
        /**Obtenemos fecha**/
        Calendar fechaAhora = Calendar.getInstance(TimeZone.getDefault());
        Calendar horaAhora = Calendar.getInstance(TimeZone.getDefault());
        Calendar fechaReturn = Calendar.getInstance(TimeZone.getDefault());
        fechaReturn.setTimeInMillis(0);
        int dia = fechaAhora.get(Calendar.DAY_OF_WEEK);
        horaAhora.set(Calendar.DAY_OF_MONTH,1);
        horaAhora.set(Calendar.MONTH, Calendar.JANUARY);
        horaAhora.set(Calendar.YEAR, 1970);

        long ha =  horaAhora.getTimeInMillis();


        switch (dia) {
            case Calendar.MONDAY:
                if(tarea.isLunes() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isMartes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isMiercoles()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isJueves()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isViernes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isSabado()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isDomingo()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
            case Calendar.TUESDAY:
                if(tarea.isMartes() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isMiercoles()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isJueves()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isViernes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isSabado()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isDomingo()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isLunes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
            case Calendar.WEDNESDAY:
                if(tarea.isMiercoles() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isJueves()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isViernes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isSabado()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isDomingo()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isLunes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isMartes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
            case Calendar.THURSDAY:
                if(tarea.isJueves() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isViernes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isSabado()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isDomingo()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isLunes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isMartes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isMiercoles()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
            case Calendar.FRIDAY:
                if(tarea.isViernes() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isSabado()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isDomingo()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isLunes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isMartes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isMiercoles()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isJueves()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
            case Calendar.SATURDAY:
                if(tarea.isSabado() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isDomingo()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isLunes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isMartes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isMiercoles()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isJueves()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isViernes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
            case Calendar.SUNDAY:
                if(tarea.isDomingo() && horaAhora.getTimeInMillis()<tarea.getHora()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                } else if(tarea.isLunes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,1);
                } else if(tarea.isMartes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,2);
                } else if(tarea.isMiercoles()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,3);
                } else if(tarea.isJueves()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,4);
                } else if(tarea.isViernes()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,5);
                } else if(tarea.isSabado()){
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,6);
                } else {
                    fechaReturn.setTimeInMillis(fechaAhora.getTimeInMillis());
                    fechaReturn.add(Calendar.DAY_OF_MONTH,7);
                }
                break;
        }

        /*
        fechaReturn.set(Calendar.HOUR_OF_DAY,0);
        fechaReturn.set(Calendar.MINUTE,0);
        fechaReturn.set(Calendar.SECOND,0);
        fechaReturn.set(Calendar.MILLISECOND,0);
        */

        return fechaReturn;
    }

    /**
     * Comprobacion de igualdad entre dos fechas obviando segundos y milisegundos.
     * @param horaFecha1 valor1 long en milisegundos.
     * @param horaFecha2 valor2 long en milisegundos.
     * @return true=es igual. false=es diferente.
     */
    static public boolean verificarMismaFechaHoraMinuto(long horaFecha1, long horaFecha2){
        Calendar cHoraFecha1 = Calendar.getInstance(TimeZone.getDefault());
        cHoraFecha1.setTimeInMillis(horaFecha1);
        Calendar cHoraFecha2 = Calendar.getInstance(TimeZone.getDefault());
        cHoraFecha2.setTimeInMillis(horaFecha2);

        cHoraFecha2.set(Calendar.SECOND,0);
        cHoraFecha2.set(Calendar.MILLISECOND,0);
        cHoraFecha1.set(Calendar.SECOND,0);
        cHoraFecha1.set(Calendar.MILLISECOND,0);

        if( cHoraFecha2.getTimeInMillis() == cHoraFecha1.getTimeInMillis() )
            return true;
        else
            return false;
    }
}
