package esei.uvigo.es.lilla.receivers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.view.WindowManager;

import java.util.Calendar;
import java.util.TimeZone;

import esei.uvigo.es.lilla.Lilla;
import esei.uvigo.es.lilla.ListaTareasProgramadas;
import esei.uvigo.es.lilla.MainActivity;
import esei.uvigo.es.lilla.MostrarAlarma;
import esei.uvigo.es.lilla.database.TareasBD;
import esei.uvigo.es.lilla.abstract_classes.FunctionsAlarmManager;
import esei.uvigo.es.lilla.entities.NivelImportanciaTarea;
import esei.uvigo.es.lilla.entities.TareaNotificada;
import esei.uvigo.es.lilla.entities.TareaPuntual;

/**
 * Created by Oscar Campos del Río on 12/02/2017.
 */

/**
 * Receiver alarma establecida en AlarmManager para mostrar alarma de tarea puntual.
 */
public class ReceiverAlarmTareaPuntual extends WakefulBroadcastReceiver {
    private TareaPuntual tarea;
    private TareaNotificada tareaNotificada;
    private NivelImportanciaTarea nivelImportanciaTarea;
    private int idTareaNotificada;

    @Override
    public void onReceive(Context context, android.content.Intent i) {
        Calendar fechaHoraNotificacion = Calendar.getInstance(TimeZone.getDefault());
        Bundle params = i.getExtras();
        Calendar fechaHoraAlarma = Calendar.getInstance(TimeZone.getDefault());
        fechaHoraAlarma.setTimeInMillis(params.getLong("fechaHora"));

        /* Borramos tarea base de datos y Añadimos tarea a tareas notificadas. */
        tarea =  ((Lilla) context.getApplicationContext()).tareasBD.getTareaPuntual(params.getInt("id"));
        nivelImportanciaTarea = ((Lilla) context.getApplicationContext()).tareasBD.getNivelImportanciaTarea(tarea.getNivel_importancia());
        fechaHoraNotificacion.add(Calendar.MINUTE, nivelImportanciaTarea.getMinutos());
        tareaNotificada = new TareaNotificada(tarea.getId_tarea(),tarea.getTitulo(),tarea.getDescripcion(),
                fechaHoraAlarma.getTimeInMillis(),0L,fechaHoraNotificacion.getTimeInMillis(),
                tarea.getNivel_importancia(),0,false);
        idTareaNotificada = ((Lilla) context.getApplicationContext()).tareasBD.nuevaTareaNotificada(tareaNotificada);
        tareaNotificada.setId_tarea_notificada(idTareaNotificada);

        ((Lilla) context.getApplicationContext()).tareasBD.borrarTarea(params.getInt("id"));


        /* Si estan instanciados ReciclersViews actualizamos contenido. */
        if(MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter != null ){
            MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.setList( ((Lilla) context.getApplicationContext()).tareasBD.getAllTareasSinConfirmar());
            MainActivity.PlaceholderFragment.tareasSinConfirmarAdapter.notifyDataSetChanged();
        }

        if(ListaTareasProgramadas.tareasAdapter != null ) {
            ListaTareasProgramadas.tareasAdapter.setList(((Lilla) context.getApplicationContext()).tareasBD.getAllTareas());
            ListaTareasProgramadas.tareasAdapter.notifyDataSetChanged();
        }


        /* Generamos alarma para notificaciones de la tarea puntual. */
        FunctionsAlarmManager.establecerNotificacionTareaSinConfirmar(  tareaNotificada,nivelImportanciaTarea.getMinutos(),
                                                            nivelImportanciaTarea.getLimite_notificaciones(),
                                                            nivelImportanciaTarea.isAviso_cp(), context);


        /* Preparamos intent para lanzar activity y mostrar alarma de la tarea. */
        Intent alarmIntent  = new Intent(context, MostrarAlarma.class);
        alarmIntent.putExtra("id", tarea.getId_tarea());
        alarmIntent.putExtra("titulo",tarea.getTitulo());
        alarmIntent.putExtra("hora",params.getLong("fechaHora"));
        alarmIntent.putExtra("color",nivelImportanciaTarea.getColor());
        alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK + Intent.FLAG_ACTIVITY_NO_HISTORY);
        alarmIntent.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED +
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD +
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON +
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        context.startActivity(alarmIntent);


    }
}

