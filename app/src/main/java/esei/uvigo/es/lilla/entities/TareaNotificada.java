package esei.uvigo.es.lilla.entities;

import android.os.Build;

import java.security.InvalidParameterException;

import static java.util.Objects.requireNonNull;

/**
 * Created by Oscar Campos del Río on 25/01/2017.
 */

/**
 * Clase con datos de tarea que ya ha sido notificada mediante alarma.
 */
public class TareaNotificada {
    /**
     * Atributo identificador de la tarea-
     */
    private int id_tarea_notificada;

    /**
     * Atributo cadena de caracteres con el titulo de la tarea.
     */
    private String titulo;

    /**
     * Atributo cadena de caracteres con la drescripción de la tarea..
     */
    private String descripcion;

    /**
     * Atributo representa hora y fecha d en que debe ser realizada la tarea.(valor en milisegundos
     * desde 01/01/1970).
     */
    private long fechaHoraTarea;

    /**
     * Atributo representa hora y fecha de realizaición de tarea. Usado para ejecución de la alarma
     * (valor en milisegundos desde 01/01/1970. Si valor 0 y confirmado=false es que no se realizo).
     */
    private long fechaHoraRealizado;

    /**
     * Atributo representa fecha y hora de siguiente notificación de la tarea (valor en milisegundos
     * desde 01/01/1970. Este atributo se reasigna cuando la app está en ejecución.
     */
    private long fechaHoraNotificacion;

    /**
     * Atributo identificador del nivel de importancia de la tarea.
     */
    private int nivel_importancia;

    /**
     * Atributo cantidad de notificaciones que se han realizado de la tarea (Se reinicia a 0 cuando
     * la tarea se marca como confirmada, para volver a notificar como una tarea notificada
     * realizada).
     */
    private int cantidad_notificaciones;

    /**
     * Atributo boleano que indica si la TareaNotificada esta realizada.
     */
    private boolean confirmado;

    public TareaNotificada(int id_tarea_notificada, String titulo, String descripcion,
                           long fechaHoraTarea, long fechaHoraRealizado,
                           long fechaHoraNotificacion, int nivel_importancia,
                           int cantidad_notificaciones, boolean confirmado) {
        this.setId_tarea_notificada(id_tarea_notificada);
        this.setTitulo(titulo);
        this.setDescripcion(descripcion);
        this.setFechaHoraTarea(fechaHoraTarea);
        this.setFechaHoraRealizado(fechaHoraRealizado);
        this.setFechaHoraNotificacion(fechaHoraNotificacion);
        this.setNivel_importancia(nivel_importancia);
        this.setCantidad_notificaciones(cantidad_notificaciones);
        this.setConfirmado(confirmado);
    }

    public int getId_tarea_notificada() {
        return id_tarea_notificada;
    }

    public void setId_tarea_notificada(int id_tarea_notificada) throws InvalidParameterException {
        if(id_tarea_notificada>=0)
            this.id_tarea_notificada = id_tarea_notificada;
        else
            throw new InvalidParameterException();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) throws NullPointerException {
        if(Build.VERSION.SDK_INT >= 19)
            this.titulo = requireNonNull(titulo, "titulo can not be null");
        else {
            this.titulo=titulo;
            if(this.titulo==null) throw new NullPointerException();
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        if(descripcion==null){
            this.descripcion="";
        } else
            this.descripcion=descripcion;
    }

    public long getFechaHoraTarea() {
        return fechaHoraTarea;
    }

    public void setFechaHoraTarea(long fechaHoraTarea) {
            this.fechaHoraTarea = fechaHoraTarea;
    }

    public int getNivel_importancia() {
        return nivel_importancia;
    }

    public void setNivel_importancia(int nivel_importancia)  {
        if(nivel_importancia>=0)
            this.nivel_importancia = nivel_importancia;
        else
            throw new InvalidParameterException();
    }

    public int getCantidad_notificaciones() {
        return cantidad_notificaciones;
    }

    public void setCantidad_notificaciones(int cantidad_notificaciones)  throws InvalidParameterException {
        if(cantidad_notificaciones>=0)
            this.cantidad_notificaciones = cantidad_notificaciones;
        else
            throw new InvalidParameterException();
    }

    public boolean isConfirmado() {
        return confirmado;
    }

    public void setConfirmado(boolean confirmado) {
        this.confirmado = confirmado;
    }

    public long getFechaHoraRealizado() {
        return fechaHoraRealizado;
    }

    public void setFechaHoraRealizado(long fechaHoraRealizado) {
            this.fechaHoraRealizado = fechaHoraRealizado;
    }

    public long getFechaHoraNotificacion() {
        return fechaHoraNotificacion;
    }

    public void setFechaHoraNotificacion(long fechaHoraNotificacion) {
            this.fechaHoraNotificacion = fechaHoraNotificacion;
    }

    @Override
    public String toString() {
        return "TareaNotificada{" +
                "id_tarea_notificada=" + id_tarea_notificada +
                ", titulo='" + titulo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", fechaHoraTarea=" + fechaHoraTarea +
                ", fechaHoraRealizado=" + fechaHoraRealizado +
                ", fechaHoraNotificacion=" + fechaHoraNotificacion +
                ", nivel_importancia=" + nivel_importancia +
                ", cantidad_notificaciones=" + cantidad_notificaciones +
                ", confirmado=" + confirmado +
                '}';
    }    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TareaNotificada other = (TareaNotificada) obj;
        if (id_tarea_notificada != other.id_tarea_notificada)
            return false;
        return true;
    }




}
