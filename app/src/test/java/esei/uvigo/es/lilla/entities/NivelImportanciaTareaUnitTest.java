package esei.uvigo.es.lilla.entities;

import org.junit.Test;

import java.security.InvalidParameterException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Oscar Campos del Río on 19/03/2017.
 */

public class NivelImportanciaTareaUnitTest {

    final private int id_nivel = 0;
    final private String nombre = "super dangerous";
    final private int limite_notificaciones = 10;
    final private boolean aviso_cp = true;
    final private int minutos = 30;
    final private String color = "afeeee";

    @Test
    public void testNivelImportanciaTareaIntStringIntBooleanIntString() {
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color)));
    }


    @Test(expected = InvalidParameterException.class)
    public void testNivelImportanciaTareaIntStringIntBooleanIntStringInvalidId_nivel() {
        new NivelImportanciaTarea(-1,nombre,limite_notificaciones,aviso_cp,minutos,color);
    }

    @Test(expected = NullPointerException.class)
    public void testNivelImportanciaTareaIntStringIntBooleanIntStringNullNombre() {
        new NivelImportanciaTarea(id_nivel,null,limite_notificaciones,aviso_cp,minutos,color);
    }

    @Test(expected = InvalidParameterException.class)
    public void testNivelImportanciaTareaIntStringIntBooleanIntStringInvalidLimite_notificaciones() {
        new NivelImportanciaTarea(id_nivel,nombre,-1,aviso_cp,minutos,color);
    }

    @Test(expected = InvalidParameterException.class)
    public void testNivelImportanciaTareaIntStringIntBooleanIntStringInvalidMinutos() {
        new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,-1,color);
    }

    @Test(expected = NullPointerException.class)
    public void testNivelImportanciaTareaIntStringIntBooleanIntStringNullColor() {
        new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,null);
    }

    @Test
    public void testSetId_nivel() {
        final int id_nivel_nuevo = 1;
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        nivelImportanciaTarea.setId_nivel(id_nivel_nuevo);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel_nuevo)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color)));
    }

    @Test
    public void testSetNombre() {
        final String nombre_nuevo = "mega dangerous";
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        nivelImportanciaTarea.setNombre(nombre_nuevo);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre_nuevo)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color)));
    }

    @Test
    public void testSetLimite_notificaciones() {
        final int limite_notificaciones_nuevo = 11;
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        nivelImportanciaTarea.setLimite_notificaciones(limite_notificaciones_nuevo);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones_nuevo)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color)));
    }

    @Test
    public void testSetAviso_cp() {
        final boolean aviso_cp_nuevo = false;
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        nivelImportanciaTarea.setAviso_cp(aviso_cp_nuevo);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp_nuevo)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color)));
    }


    @Test
    public void testSetMinutos() {
        final int minutos_nuevo = 40;
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        nivelImportanciaTarea.setMinutos(minutos_nuevo);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos_nuevo)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color)));
    }

    @Test
    public void testSetColor() {
        final String color_nuevo = "ffffff";
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);

        nivelImportanciaTarea.setColor(color_nuevo);

        assertThat(nivelImportanciaTarea.getId_nivel(), is(equalTo(id_nivel)));
        assertThat(nivelImportanciaTarea.getNombre(), is(equalTo(nombre)));
        assertThat(nivelImportanciaTarea.getLimite_notificaciones(),is(equalTo(limite_notificaciones)));
        assertThat(nivelImportanciaTarea.isAviso_cp(), is(equalTo(aviso_cp)));
        assertThat(nivelImportanciaTarea.getMinutos(), is(equalTo(minutos)));
        assertThat(nivelImportanciaTarea.getColor(), is(equalTo(color_nuevo)));
    }


    @Test(expected = InvalidParameterException.class)
    public void testInvalidId_nivel() {
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);
        nivelImportanciaTarea.setId_nivel(-1);
    }

    @Test(expected = NullPointerException.class)
    public void testNullNombre() {
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);
        nivelImportanciaTarea.setNombre(null);
    }

    @Test(expected = InvalidParameterException.class)
    public void testInvalidLimite_notificaciones() {
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);
        nivelImportanciaTarea.setLimite_notificaciones(-1);
    }

    @Test(expected = InvalidParameterException.class)
    public void testInvalidMinutos() {
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);
        nivelImportanciaTarea.setMinutos(-1);
    }

    @Test(expected = NullPointerException.class)
    public void testNullColor() {
        final NivelImportanciaTarea nivelImportanciaTarea = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);
        nivelImportanciaTarea.setColor(null);
    }

    @Test
    public void testEqualsObject() {
        final NivelImportanciaTarea nivelImportanciaTarea1 = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);
        final NivelImportanciaTarea nivelImportanciaTarea2 = new NivelImportanciaTarea(id_nivel,nombre,limite_notificaciones,aviso_cp,minutos,color);


        assertTrue(nivelImportanciaTarea1.equals(nivelImportanciaTarea2));
    }

}
