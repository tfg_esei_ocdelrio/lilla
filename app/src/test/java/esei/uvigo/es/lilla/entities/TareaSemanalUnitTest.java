package esei.uvigo.es.lilla.entities;

import org.junit.Test;

import java.security.InvalidParameterException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Oscar Campos del Río on 21/03/2017.
 */

public class TareaSemanalUnitTest {
    final private int id_tarea = -1;
    final private String titulo = "play pokemon";
    final private String descripcion = "play pokemon all the day";
    final private int nivel_importancia = 0;
    final private boolean lunes = true;
    final private boolean martes = true;
    final private boolean miercoles = true;
    final private boolean jueves = true;
    final private boolean viernes = true;
    final private boolean sabado = true;
    final private boolean domingo = true;
    final private long hora = 3600000L;
    final private long  fechaHoraSigAlarma = 7300000L;

    @Test
    public void testTareaIntStringStringLongIntBooleanBooleanBooleanBooleanBooleanBooleanBooleanLong() {
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }


    @Test
    public void testSetLunes(){
        final boolean lunes_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setLunes(lunes_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes_nuevo)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetMartes(){
        final boolean martes_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setMartes(martes_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes_nuevo)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetMiercoles(){
        final boolean miercoles_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setMiercoles(miercoles_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles_nuevo)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetJueves(){
        final boolean jueves_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setJueves(jueves_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves_nuevo)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetViernes(){
        final boolean viernes_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setViernes(viernes_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes_nuevo)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetSabado(){
        final boolean sabado_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setSabado(sabado_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado_nuevo)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetDomingo(){
        final boolean domingo_nuevo = false;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setDomingo(domingo_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo_nuevo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma)));
    }

    @Test
    public void testSetHora(){
        final long hora_nueva= 1L;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setFechaHoraSigAlarma(hora_nueva);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(hora_nueva)));


    }

    @Test
    public void testSetFechaHoraSigAlarma(){
        final long fechaHoraSigAlarma_nuevo= 1L;
        final TareaSemanal tarea = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        tarea.setFechaHoraSigAlarma(fechaHoraSigAlarma_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getHora(), is(equalTo(hora)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.isLunes(), is(equalTo(lunes)));
        assertThat(tarea.isMartes(), is(equalTo(martes)));
        assertThat(tarea.isMiercoles(), is(equalTo(miercoles)));
        assertThat(tarea.isJueves(), is(equalTo(jueves)));
        assertThat(tarea.isViernes(), is(equalTo(viernes)));
        assertThat(tarea.isSabado(), is(equalTo(sabado)));
        assertThat(tarea.isDomingo(), is(equalTo(domingo)));
        assertThat(tarea.getFechaHoraSigAlarma(), is(equalTo(fechaHoraSigAlarma_nuevo)));


    }


    @Test
    public void testEqualsObject() {
        final TareaSemanal tarea1 = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);
        final TareaSemanal tarea2 = new TareaSemanal(id_tarea,titulo,descripcion,nivel_importancia,lunes,martes,miercoles,jueves,viernes,sabado,domingo,hora,fechaHoraSigAlarma);

        assertTrue(tarea1.equals(tarea2));

    }





}
