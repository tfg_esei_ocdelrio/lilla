package esei.uvigo.es.lilla.entities;

import org.junit.Test;
import java.security.InvalidParameterException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
/**
 * Created by Oscar Campos del Río on 20/03/2017.
 */

public class TareaNotificadaUnitTest {
    final private int id_tarea_notificada = 0;
    final private String titulo = "play pokemon";
    final private String descripcion = "play pokemon all the day";
    final private long fechaHoraTarea = 3600000L;
    final private long fechaHoraRealizado = 6400000L;
    final private long fechaHoraNotificacion = 3610000L;
    final private int nivel_importancia = 0;
    final private int cantidad_notificaciones = 0;
    final private boolean confirmado = true;

    @Test
    public void testTareaNotificadaIntStringStringLongLongLongIntIntBoolean(){
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada,titulo,descripcion,fechaHoraTarea,fechaHoraRealizado,fechaHoraNotificacion,nivel_importancia,cantidad_notificaciones,confirmado);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test(expected = InvalidParameterException.class)
    public void testTareaNotificadaIntStringStringLongLongLongIntIntBooleanInvalidId_tarea() {
        new TareaNotificada(-1, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
    }

    @Test(expected = NullPointerException.class)
    public void testTareaNotificadaIntStringStringLongLongLongIntIntBooleanNullTitulo() {
        new TareaNotificada(id_tarea_notificada, null, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
    }


    @Test(expected = InvalidParameterException.class)
    public void testTareaNotificadaIntStringStringLongLongLongIntIntBooleanInvalidNivel_importancia() {
        new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, -1, cantidad_notificaciones, confirmado);
    }

    @Test(expected = InvalidParameterException.class)
    public void testTareaNotificadaIntStringStringLongLongLongIntIntBooleanInvalidCantidad_notificaciones() {
        new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, -1, confirmado);
    }

    @Test
    public void testSetId_tarea_notificada(){
        final int id_tarea_notificada_nuevo = 2;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setId_tarea_notificada(id_tarea_notificada_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada_nuevo)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetTitulo(){
        final String titulo_nuevo = "play_digimon";
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setTitulo(titulo_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo_nuevo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetDescripcionNull(){
        final String descripcion_nuevo = null;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setDescripcion(descripcion_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo("")));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetDescripcion(){
        final String descripcion_nuevo = "play all day";
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setDescripcion(descripcion_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion_nuevo)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetFechaHoraTarea(){
        final long fechaHoraTarea_nuevo = 0L;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setFechaHoraTarea(fechaHoraTarea_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea_nuevo)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetFechaHoraRealizado(){
        final long fechaHoraRealizado_nuevo = 0L;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setFechaHoraRealizado(fechaHoraRealizado_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado_nuevo)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetFechaHoraNotificacion(){
        final long fechaHoraNotificacion_nuevo = 0L;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setFechaHoraNotificacion(fechaHoraNotificacion_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion_nuevo)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetNivel_importancia(){
        final int nivel_importancia_nuevo = 1;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setNivel_importancia(nivel_importancia_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia_nuevo)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));
    }

    @Test
    public void testSetCantidad_notificaciones(){
        final int cantidad_notificaciones_nuevo = 1;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setCantidad_notificaciones(cantidad_notificaciones_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones_nuevo)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado)));


    }

    @Test
    public void testSetConfirmado(){
        final boolean confirmado_nuevo = false;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        tareaNotificada.setConfirmado(confirmado_nuevo);

        assertThat(tareaNotificada.getId_tarea_notificada(), is(equalTo(id_tarea_notificada)));
        assertThat(tareaNotificada.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaNotificada.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaNotificada.getFechaHoraTarea(), is(equalTo(fechaHoraTarea)));
        assertThat(tareaNotificada.getFechaHoraRealizado(), is(equalTo(fechaHoraRealizado)));
        assertThat(tareaNotificada.getFechaHoraNotificacion(), is(equalTo(fechaHoraNotificacion)));
        assertThat(tareaNotificada.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaNotificada.getCantidad_notificaciones(), is(equalTo(cantidad_notificaciones)));
        assertThat(tareaNotificada.isConfirmado(), is(equalTo(confirmado_nuevo)));
    }

    @Test(expected = InvalidParameterException.class)
    public void testSetInvalidÏd_tarea_notificada(){;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
        tareaNotificada.setId_tarea_notificada(-1);
    }

    @Test(expected = NullPointerException.class)
    public void testSetNullTitulo() {
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
        tareaNotificada.setTitulo(null);
    }

    @Test(expected = InvalidParameterException.class)
    public void testSetInvalidNivel_importancia(){;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
        tareaNotificada.setNivel_importancia(-1);
    }

    @Test(expected = InvalidParameterException.class)
    public void testSetInvalidCantidad_notificaciones(){;
        final TareaNotificada tareaNotificada = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
        tareaNotificada.setCantidad_notificaciones(-1);
    }

    @Test
    public void testEqualsObject() {
        final TareaNotificada tareaNotificada1 = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);
        final TareaNotificada tareaNotificada2 = new TareaNotificada(id_tarea_notificada, titulo, descripcion, fechaHoraTarea, fechaHoraRealizado, fechaHoraNotificacion, nivel_importancia, cantidad_notificaciones, confirmado);

        assertTrue(tareaNotificada1.equals(tareaNotificada2));

    }







}
