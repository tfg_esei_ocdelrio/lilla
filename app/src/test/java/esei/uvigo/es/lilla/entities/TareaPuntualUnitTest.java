package esei.uvigo.es.lilla.entities;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Oscar Campos del Río on 21/03/2017.
 */

public class TareaPuntualUnitTest {
    final private int id_tarea = -1;
    final private String titulo = "play pokemon";
    final private String descripcion = "play pokemon all the day";
    final private int nivel_importancia = 0;
    final private long fecha = 554767200000L;

    @Test
    public void testTareaPuntualIntStringStringLongLongInt() {
        final TareaPuntual tarea = new TareaPuntual(id_tarea,titulo,descripcion,fecha,nivel_importancia);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tarea.getFechaHora(), is(equalTo(fecha)));
    }


    @Test
    public void testSetFecha() {
        final long fecha_nuevo = 0L;
        final TareaPuntual tareaPuntual = new TareaPuntual(id_tarea, titulo, descripcion, fecha, nivel_importancia);

        tareaPuntual.setFechaHora(fecha_nuevo);

        assertThat(tareaPuntual.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tareaPuntual.getTitulo(), is(equalTo(titulo)));
        assertThat(tareaPuntual.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tareaPuntual.getNivel_importancia(), is(equalTo(nivel_importancia)));
        assertThat(tareaPuntual.getFechaHora(), is(equalTo(fecha_nuevo)));
    }


    @Test
    public void testEqualsObject(){
        final TareaPuntual tareaPuntual1 = new TareaPuntual(id_tarea, titulo, descripcion, fecha, nivel_importancia);
        final TareaPuntual tareaPuntual2 = new TareaPuntual(id_tarea, titulo, descripcion, fecha, nivel_importancia);

        assertTrue(tareaPuntual1.equals(tareaPuntual2));
    }

}
