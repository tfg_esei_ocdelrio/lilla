package esei.uvigo.es.lilla.entities;

import org.junit.Test;

import java.security.InvalidParameterException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Oscar Campos del Río on 20/03/2017.
 */


public class TareaUnitTest {
	final private int id_tarea = -1;
    final private String titulo = "play pokemon";
    final private String descripcion = "play pokemon all the day";
    final private int nivel_importancia = 0;

    @Test
    public void testTareaIntStringStringLongInt() {
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
    }

    @Test(expected = InvalidParameterException.class)
    public void testTareaIntStringStringLongIntInvalidId_tarea() {
        new Tarea(-2,titulo,descripcion,nivel_importancia);
    }


    @Test(expected = NullPointerException.class)
    public void testTareaIntStringStringLongIntNullTitulo() {
        new Tarea(id_tarea,null,descripcion,nivel_importancia);
    }


    @Test(expected = InvalidParameterException.class)
    public void testTareaIntStringStringLongIntInvalidNivel_importancia() {
        new Tarea(id_tarea,titulo,descripcion,-1);
    }

    @Test
    public void testSetId_tarea() {
        final int id_tarea_nuevo = 2;
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        tarea.setId_tarea(id_tarea_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea_nuevo)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
    }

    @Test
    public void testSetTitulo() {
        final String titulo_nuevo = "play_digimon";
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        tarea.setTitulo(titulo_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo_nuevo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
    }

    @Test
    public void testSetDescripcionNull() {
        final String descripcion_nuevo = null;
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        tarea.setDescripcion(descripcion_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo("")));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
    }

    @Test
    public void testSetDescripcion() {
        final String descripcion_nuevo = "play all day";
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        tarea.setDescripcion(descripcion_nuevo);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion_nuevo)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia)));
    }



    @Test
    public void testSetNivel_importancia() {
        final int nivel_importancia_nueva = 0;
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        tarea.setNivel_importancia(nivel_importancia_nueva);

        assertThat(tarea.getId_tarea(), is(equalTo(id_tarea)));
        assertThat(tarea.getTitulo(), is(equalTo(titulo)));
        assertThat(tarea.getDescripcion(), is(equalTo(descripcion)));
        assertThat(tarea.getNivel_importancia(), is(equalTo(nivel_importancia_nueva)));
    }


    @Test(expected = InvalidParameterException.class)
    public void testSetInvalidId_tarea() {
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);
        tarea.setId_tarea(-2);
    }

    @Test(expected = NullPointerException.class)
    public void testSetNullTitulo() {
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);
        tarea.setTitulo(null);
    }

    @Test(expected = InvalidParameterException.class)
    public void testSetInvalidNivel_importancia() {
        final Tarea tarea = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);
        tarea.setNivel_importancia(-1);
    }

    @Test
    public void testEqualsObject() {
        final Tarea tarea1 = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);
        final Tarea tarea2 = new Tarea(id_tarea,titulo,descripcion,nivel_importancia);

        assertTrue(tarea1.equals(tarea2));
    }








}